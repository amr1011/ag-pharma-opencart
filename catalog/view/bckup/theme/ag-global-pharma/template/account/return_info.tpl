<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/account.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/return.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
          <?php $bcount = count($breadcrumbs); ?>
          <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
          <?php if($bkey > 0): ?>
          <h2><?php echo $breadcrumb['text']; ?></h2>
          <?php if($bcount != ($bkey+1)): ?>
            <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
          <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
          <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel return-info-panel">

          <div class="col-sm-8 returninfo">
            
            <!-- wishlist -->

            <div class="box-panel ">
              <p class="title-content"><?php echo $text_return_detail; ?></p>
              <div class="para-content padding-0px">
                <table class="return-info-tbl first-tbl">
                  <tbody>
                    <tr>
                      <td>
                        <div>
                          <p><span><?php echo $text_return_id; ?> </span><?php echo $return_id; ?></p>
                          <p><span><?php echo $text_date_added; ?> </span> <?php echo $date_added; ?></p>
                        </div>
                      </td>
                      <td>
                        <div>
                          <p><span><?php echo $text_order_id; ?> </span> <?php echo $order_id; ?></p>
                          <p><span><?php echo $text_date_ordered; ?> </span> <?php echo $date_ordered; ?></p>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
            </div>


            <div class="box-panel margin-top-50 ">
              <p class="title-content"><?php echo $text_product; ?></p>
              <div class="para-content padding-0px">
                <table class="return-info-tbl second-tbl">
                  <tbody>
                    <tr>
                      <td>
                        <div class="change-div">
                          <p class="change-text"><?php echo $column_product; ?></p>                      
                        </div>
                      </td>
                      <td>
                        <div>
                          <p><?php echo $column_model; ?></p>                      
                        </div>
                      </td>
                      <td>
                        <div>
                          <p><?php echo $column_quantity; ?></p>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div>
                          <p><?php echo $product; ?></p>
                        </div>
                      </td>
                      <td>
                        <div>
                          <p><?php echo $model; ?></p>                      
                        </div>
                      </td>
                      <td>
                        <div>
                          <p><?php echo $quantity; ?></p>
                        </div>
                      </td>
                    </tr>
                    
                  </tbody>
                </table>
              </div>
              
            </div>


            <div class="box-panel margin-top-50 ">        
              <table class="return-info-tbl third-tbl">
                <tbody>
                  <tr>
                    <td>
                      <div>
                        <p><?php echo $column_reason; ?></p>                     
                      </div>
                    </td>               
                    <td>
                      <div>
                        <p><?php echo $column_opened; ?></p>                     
                      </div>
                    </td>
                    <td>
                      <div>
                        <p><?php echo $column_action; ?></p>                     
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div>
                        <p><?php echo $reason; ?></p>                      
                      </div>
                    </td>               
                    <td>
                      <div>
                        <p><?php echo $opened; ?></p>                     
                      </div>
                    </td>
                    <td>
                      <div>
                        <p><?php echo $action; ?></p>                     
                      </div>
                    </td>
                  </tr>             
                </tbody>
              </table>          
            </div>
            <?php if ($comment) { ?>
            <div class="box-panel margin-top-50 ">
              <p class="title-content"><?php echo $text_comment; ?></p>
              <div class="para-content padding-0px">
                <table class="return-info-tbl fourth-tbl">
                  <tbody>
                    <tr>
                      <td>
                        <div>
                          <p><?php echo $comment; ?></p>                      
                        </div>
                      </td>               
                    </tr>             
                  </tbody>
                </table>
              </div>
              
            </div>
            <?php } ?>

            <div class="f-right margin-top-10 margin-bottom-30 ">
              <a href="<?php echo $continue; ?>">
                <button class="ag-button"><?php echo $button_continue; ?></button>
              </a>
            </div>
            <div class="clear"></div>
          </div>              

          <!-- menu panel -->
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>