<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/addressbook.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
          <?php $bcount = count($breadcrumbs); ?>
          <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
          <?php if($bkey > 0): ?>
          <h2><?php echo $breadcrumb['text']; ?></h2>
          <?php if($bcount != ($bkey+1)): ?>
            <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
          <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
          <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel full-height addressbook-full-panel">

          <div class="col-sm-8 addressbook">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>            
            <!-- wishlist -->

            <div class="box-panel margin-top-30">
              <p class="title-content"><?php echo $text_address_book; ?></p>
              <div class="para-content padding-20px">
                <?php if ($addresses) { ?>
                  <?php foreach ($addresses as $result) { ?>
                    <div class="f-left font-15">
                      <?php echo $result['address']; ?>
                    </div>
                    <div class="f-right button-div">
                      <a href="<?php echo $result['update']; ?>">
                        <button class="ag-button margin-right-20"><?php echo $button_edit; ?></button>
                      </a>
                      <button class="ag-button" onclick="window.location.href='<?php echo $result['delete']; ?>';"><?php echo $button_delete; ?></button>
                    </div>
                    <div class="clear"></div>
                  <?php } ?>
                <?php } else { ?>
                <p><?php echo $text_empty; ?></p>
                <?php } ?>
              </div>
              
            </div>
            <div class="f-left margin-top-10">
              <button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';"><?php echo $button_back; ?></button>
            </div>
            <div class="f-right margin-top-10">
              <button class="ag-button width-150px" onclick="window.location.href='<?php echo $add; ?>';"><?php echo $button_new_address; ?></button>
            </div>
            <div class="clear"></div>
          </div>              
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>