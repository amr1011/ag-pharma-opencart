<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/news.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
        <?php else: ?>
          <?php echo $breadcrumb['text']; ?>
        <?php endif; ?>
        <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel full-height news-full-panel">

          <div class="col-sm-8 news">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <!-- wishlist -->

            <div class="box-panel margin-top-30">
              <p class="title-content"><?php echo $heading_title; ?></p>
              <div class="para-content padding-20px">
                <div class="padding-left-50">
                  <p class="display-inline-mid font-15 margin-right-20"><?php echo $entry_newsletter; ?></p>
                  <?php if ($newsletter) { ?>
                  <div class="display-inline-mid margin-right-10">
                    <input type="radio" name="newsletter" id="yes" class="width-20px default-cursor" checked="checked" value="1">
                    <label for="yes" class="default-cursor"><?php echo $text_yes; ?></label>
                  </div>
                  <div class="display-inline-mid">
                    <input type="radio" name="newsletter" id="no" class="width-20px default-cursor" value="0">
                    <label for="no" class="default-cursor"><?php echo $text_no; ?></label>
                  </div>
                  <?php } else { ?>
                  <div class="display-inline-mid margin-right-10">
                    <input type="radio" name="newsletter" id="yes" class="width-20px default-cursor" value="1">
                    <label for="yes" class="default-cursor"><?php echo $text_yes; ?></label>
                  </div>
                  <div class="display-inline-mid">
                    <input type="radio" name="newsletter" id="no" class="width-20px default-cursor" value="0" checked="checked">
                    <label for="no" class="default-cursor"><?php echo $text_no; ?></label>
                  </div>
                  <?php } ?>
                </div>
              </div>
              
            </div>
            <div class="f-left margin-top-10 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </form>
          </div>
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>