<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/registration.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
      <?php else: ?>
        <?php echo $breadcrumb['text']; ?>
      <?php endif; ?>
        <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel big-regs-panel">

          <div class="col-sm-8 regs-panel">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <p class="font-30"><?php echo $heading_title; ?></p>
            <p class="font-15 f-left"><?php echo $text_account_already; ?></p>
            <p class="font-15 f-right">Fields with <span class="red-color-ag font-20 font-bold ">* </span> are required</p>
            <div class="clear"></div>
            <?php if ($error_warning) { ?>
              <br /><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
            <?php } ?>
            <div class="box-panel margin-top-20 regs-panel1">
              <p class="title-content"><?php echo $text_your_details; ?></p>
              <div class="para-content ">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_firstname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" />
                        <?php if ($error_firstname) { ?>
                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_lastname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" />
                        <?php if ($error_lastname) { ?>
                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">*</span> <?php echo $entry_email; ?></p>
                      </td>
                      <td>
                        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" />
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_telephone; ?></p>
                      </td>
                      <td>
                        <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" />
                        <?php if ($error_telephone) { ?>
                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5" ><?php echo $entry_fax; ?></p>
                      </td>
                      <td>
                        <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>" />
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>
            </div>

            <!-- address  -->
            <div class="box-panel margin-top-20 regs-panel2">
              <p class="title-content"><?php echo $text_your_address; ?></p>
              <div class="para-content ">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_company; ?></p>
                      </td>
                      <td>
                        <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_address_1; ?></p>
                      </td>
                      <td>
                        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>" />
                        <?php if ($error_address_1) { ?>
                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><?php echo $entry_address_2; ?></p>
                      </td>
                      <td>
                        <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>" />
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_city; ?></p>
                      </td>
                      <td>
                        <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>" />
                        <?php if ($error_city) { ?>
                        <div class="text-danger"><?php echo $error_city; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_postcode; ?></p>
                      </td>
                      <td>
                        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>" />
                        <?php if ($error_postcode) { ?>
                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_country; ?></p>
                      </td>
                      <td>
                        <!-- <div class="select margin-top-5 width-100percent"> -->
                          <select name="country_id" id="input-country" class="form-control" style="margin-top:5px;">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                          </select>
                        <!-- </div> -->
                        <?php if ($error_country) { ?>
                        <div class="text-danger"><?php echo $error_country; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_zone; ?></p>
                      </td>
                      <td>
                        <!-- <div class="select margin-top-5 width-100percent"> -->
                          <select name="zone_id" id="input-zone" class="form-control" style="margin-top:5px;"></select>
                        <!-- </div> -->
                        <?php if ($error_zone) { ?>
                        <div class="text-danger"><?php echo $error_zone; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                  </tbody>
                </table>

              </div>
            </div>

            <!-- password -->
            <div class="box-panel margin-top-20 regs-panel3">
              <p class="title-content"><?php echo $text_your_password; ?></p>
              <div class="para-content ">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_password; ?></p>
                      </td>
                      <td>
                        <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" />
                        <?php if ($error_password) { ?>
                        <div class="text-danger"><?php echo $error_password; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span> <?php echo $entry_confirm; ?></p>
                      </td>
                      <td>
                        <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" />
                    <?php if ($error_confirm) { ?>
                    <div class="text-danger"><?php echo $error_confirm; ?></div>
                    <?php } ?>
                      </td>
                    </tr>             
                  </tbody>
                </table>
              </div>
            </div>

            <!-- newsletter -->
            <div class="box-panel margin-top-20 regs-panel4">
              <p class="title-content"><?php echo $text_newsletter; ?></p>
              <div class="para-content padding-30px">
                <p class="display-inline-mid margin-left-40 font-15"><?php echo $entry_newsletter; ?>: </p>
                <?php if ($newsletter) { ?>
                <div class="display-inline-mid margin-left-20">
                  <input type="radio" id="yes" name="subs" class="width-20px display-inline-mid" checked="checked">
                  <label for="yes" class="display-inline-mid margin-top-10"><?php echo $text_yes; ?></label>
                  <div class="clear"></div>
                </div>
                <div class="display-inline-mid margin-left-10">
                  <input type="radio" id="no" name="subs" class="width-20px display-inline-mid">
                  <label for="no" class="display-inline-mid margin-top-10"><?php echo $text_no; ?></label>
                  <div class="clear"></div>
                </div>
                <?php } else { ?>
                <div class="display-inline-mid margin-left-20">
                  <input type="radio" id="yes" name="subs" class="width-20px display-inline-mid">
                  <label for="yes" class="display-inline-mid margin-top-10"><?php echo $text_yes; ?></label>
                  <div class="clear"></div>
                </div>
                <div class="display-inline-mid margin-left-10">
                  <input type="radio" id="no" name="subs" class="width-20px display-inline-mid" checked="checked">
                  <label for="no" class="display-inline-mid margin-top-10"><?php echo $text_no; ?></label>
                  <div class="clear"></div>
                </div>
                <?php } ?>
              </div>


            </div>
            <div class="box-panel">
              <div class="policy">
                <?php if ($agree) { ?>
                <input type="checkbox" class="display-inline-mid width-20px margin-bottom-5 " value="1" name="agree" checked="checked">
                <?php } else { ?>
                <input type="checkbox" class="display-inline-mid width-20px margin-bottom-5 " value="1" name="agree">
                <?php } ?>
                <p class="display-inline-mid"><?php echo $text_agree; ?></p>
                <button class="ag-button"><?php echo $button_continue; ?></button>
              </div>
            </div>
            </form>
          </div>
                
          <?php echo $column_right; ?>
          <div class="clear"></div>
        </div>
      </section>
  <?php echo $content_bottom; ?>

<script type="text/javascript"><!--
// Sort the custom fields
$('#account .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
    $('#account .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#account .form-group').length) {
    $('#account .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#account .form-group').length) {
    $('#account .form-group:first').before(this);
  }
});

$('#address .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
    $('#address .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#address .form-group').length) {
    $('#address .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#address .form-group').length) {
    $('#address .form-group:first').before(this);
  }
});

$('input[name=\'customer_group_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
    dataType: 'json',
    success: function(json) {
      $('.custom-field').hide();
      $('.custom-field').removeClass('required');

      for (i = 0; i < json.length; i++) {
        custom_field = json[i];

        $('#custom-field' + custom_field['custom_field_id']).show();

        if (custom_field['required']) {
          $('#custom-field' + custom_field['custom_field_id']).addClass('required');
        }
      }


    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $(node).parent().find('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.time').datetimepicker({
  pickDate: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value=""><?php echo $text_select; ?></option>';
      html2 = '<div data-value="" class="option"><?php echo $text_select; ?></div>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';
          html2 += '<div data-value="' + json['zone'][i]['zone_id'] + '" class="option">' + json['zone'][i]['name'] + '</div>';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
        html2 += '<div data-value="0" class="option"><?php echo $text_none; ?></div>';
      }

      $('select[name=\'zone_id\']').html(html);
      $('.frm-custom-dropdown-option:last').html(html2);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'country_id\']').trigger('change');
//--></script>

<?php echo $footer; ?>