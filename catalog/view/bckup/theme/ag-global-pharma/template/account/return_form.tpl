<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/bootstrap-datetimepicker.min.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/product-form.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
          <?php $bcount = count($breadcrumbs); ?>
          <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
          <?php if($bkey > 0): ?>
          <h2><?php echo $breadcrumb['text']; ?></h2>
          <?php if($bcount != ($bkey+1)): ?>
            <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
          <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
          <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel return-form-panel">

          <div class="col-sm-8 returnform">
            <p class="font-30 font-bold"><?php echo $heading_title; ?></p>
            <p class="font-15 font-bold"><?php echo $text_description; ?></p>
            <p class="f-right font-15">Fields with <span class="red-color-ag font-20 font-bold ">* </span>are Required</p>
            <div class="clear"></div>
            <!-- wishlist -->
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="box-panel margin-top-20">
              <p class="title-content"><?php echo $text_order; ?></p>
              <div class="para-content padding-20px">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_firstname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>">
                        <?php if ($error_firstname) { ?>
                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_lastname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>">
                        <?php if ($error_lastname) { ?>
                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">*</span> <?php echo $entry_email; ?></p>
                      </td>
                      <td>
                        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>">
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_telephone; ?></p>
                      </td>
                      <td>
                        <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>">
                        <?php if ($error_telephone) { ?>
                        <div class="text-danger"><?php echo $error_telephone; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_order_id; ?></p>
                      </td>
                      <td>
                        <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>">
                        <?php if ($error_order_id) { ?>
                        <div class="text-danger"><?php echo $error_order_id; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_date_ordered; ?></p>
                      </td>
                      <td>
                        <div class="input-group width-200px margin-top-10">
                          <input class="form-control dp margin-top-0" data-date-format="YYYY-MM-DD" type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" placeholder="<?php echo $entry_date_ordered; ?>">
                          <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                        </div>
                      </td>
                    </tr>               
                  </tbody>
                </table>
              </div>
              
            </div>

            <div class="box-panel margin-top-20">
              <p class="title-content"><?php echo $text_product; ?></p>
              <div class="para-content padding-20px">
                <table class="width-100percent prod-info">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_product; ?></p>
                      </td>
                      <td>
                        <input type="text" name="product" value="<?php echo $product; ?>" placeholder="<?php echo $entry_product; ?>">
                         <?php if ($error_product) { ?>
                          <div class="text-danger"><?php echo $error_product; ?></div>
                          <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_model; ?></p>
                      </td>
                      <td>
                        <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>">
                        <?php if ($error_model) { ?>
                        <div class="text-danger"><?php echo $error_model; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><?php echo $entry_quantity; ?></p>
                      </td>
                      <td>
                        <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>">
                      </td>
                    </tr>
                    <tr>
                      <td class="move-top">
                        <p class="font-15 font-lato f-right margin-right-20 "><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_reason; ?></p>
                      </td>
                      <td class="move-down">
                        <?php foreach ($return_reasons as $return_reason) { ?>
                          <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
                          <div>
                            <input type="radio" id="dead" name="return_reason_id" class="width-20px remove-check" value="<?php echo $return_reason['return_reason_id']; ?>" checked="checked">
                            <label for="dead" class="default-cursor"><?php echo $return_reason['name']; ?></label>
                          </div>
                          <?php } else { ?>
                          <div>
                            <input type="radio" id="faulty" name="return_reason_id" class="width-20px remove-check" value="<?php echo $return_reason['return_reason_id']; ?>">
                            <label for="faulty" class="default-cursor"><?php echo $return_reason['name']; ?></label>
                          </div>
                          <?php  } ?>
                          <?php  } ?>
                          <?php if ($error_reason) { ?>
                          <div class="text-danger"><?php echo $error_reason; ?></div>
                          <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="product-open">
                        <p class="font-15 font-lato f-right margin-right-20 "><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_opened; ?></p>
                      </td>
                      <td class="product-open-radio">
                        <?php if ($opened) { ?>
                        <div class="display-inline-mid margin-right-20">
                          <input type="radio" name="opened" id="yes" class="width-20px " value="1" checked="checked">
                          <label for="yes" class="default-cursor"> <?php echo $text_yes; ?></label>
                        </div>
                        <?php } else { ?>
                        <div class="display-inline-mid margin-right-20">
                          <input type="radio" name="opened" id="yes" class="width-20px " value="1">
                          <label for="yes" class="default-cursor"> <?php echo $text_yes; ?></label>
                        </div>
                        <?php } ?>
                        <?php if (!$opened) { ?>
                        <div class="display-inline-mid">
                          <input type="radio" name="open" id="no" class="width-20px" value="0" checked="checked">
                          <label for="no" class="default-cursor"><?php echo $text_no; ?></label>
                        </div>
                        <?php } else { ?>
                        <div class="display-inline-mid">
                          <input type="radio" name="open" id="no" class="width-20px" value="0">
                          <label for="no" class="default-cursor"><?php echo $text_no; ?></label>
                        </div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td class="move-top-text">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-10"><?php echo $entry_fault_detail; ?></p>
                      </td>
                      <td class="move-down-text">
                        <textarea name="comment" rows="7" cols="50" class="big-text" placeholder="<?php echo $entry_fault_detail; ?>"><?php echo $comment; ?></textarea>
                      </td>           
                  </tbody>
                </table>
              </div>
              
                    </tr>   
            </div>
            <div class="f-left margin-top-10 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <!-- <a href="product-return-sent.html"> -->
                <button class="ag-button"><?php echo $button_submit; ?></button>
              <!-- </a> -->
            </div>
            <div class="clear"></div>
          </div>              
        </form>
          <!-- menu panel -->
          <?php echo $column_right; ?>
          
          <div class="clear"></div>   
        </div>    
      </section>

  <script>
   // $("select").transformDD()

    //datepicker
      $(".dp:not(.time)").datetimepicker({pickTime: false});
      $(".dp.time").datetimepicker({pickDate: false});

      $(".other-text").off("click").on("click", function() {        
        $(".popup-other").css({'display':'block'});                
      });
      $(".remove-check").off("click").on("click", function() {
        $(".popup-other").css({'display':'none'});                 
      });

  </script>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>