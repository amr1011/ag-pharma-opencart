<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/account-edit.css">
<?php echo $content_top; ?>
<?php echo $column_left; ?>
  <section class="reglar-panel" id="first-panel">
    <div class="heading-panel">
      <?php $bcount = count($breadcrumbs); ?>
      <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
      <?php if($bkey > 0): ?>
      <h2><?php echo $breadcrumb['text']; ?></h2>
      <?php if($bcount != ($bkey+1)): ?>
        <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
      <?php endif; ?>
      <?php else: ?>
        <?php echo $breadcrumb['text']; ?>
      <?php endif; ?>
      <?php } ?>
    </div>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>


    <!-- white panel -->
    <div class="full-panel full-height wishlist-full-panel">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
      <div class="col-sm-8 panel1">
        <p class="font-30 "><?php echo $heading_title; ?></p>
        <!-- wishlist -->
        <p class="f-right margin-bottom-10 font-15">Fields with <span class="red-color-ag font-20 font-bold ">* </span> are required</p>
        <div class="clear"></div>
        <div class="box-panel ">
          <p class="title-content"><?php echo $text_your_details; ?></p>
          <div class="para-content padding-20px">
            <table class="width-100percent">
              <tbody>
                <tr>
                  <td class="width-200px">
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_firstname; ?></p>
                  </td>
                  <td>
                    <input type="text" placeholder="<?php echo $entry_firstname; ?>" value="<?php echo $firstname; ?>" name="firstname">
                    <?php if ($error_firstname) { ?>
                    <div class="text-danger"><?php echo $error_firstname; ?></div>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_lastname; ?></p>
                  </td>
                  <td>
                    <input type="text" placeholder="<?php echo $entry_lastname; ?>" value="<?php echo $lastname; ?>" name="lastname">
                    <?php if ($error_lastname) { ?>
                    <div class="text-danger"><?php echo $error_lastname; ?></div>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">*</span> <?php echo $entry_email; ?></p>
                  </td>
                  <td>
                    <input type="text" placeholder="E-Mail Address" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>">
                    <?php if ($error_email) { ?>
                    <div class="text-danger"><?php echo $error_email; ?></div>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_telephone; ?></p>
                  </td>
                  <td>
                    <input type="text" placeholder="Telephone Number" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>">
                    <?php if ($error_telephone) { ?>
                    <div class="text-danger"><?php echo $error_telephone; ?></div>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><?php echo $entry_fax; ?></p>
                  </td>
                  <td>
                    <input type="text" name="fax" value="<?php echo $fax; ?>" placeholder="<?php echo $entry_fax; ?>">
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          
        </div>
        <div class="f-left margin-top-10 margin-bottom-30">
          <button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
        </div>
        <div class="f-right margin-top-10 margin-bottom-30">
          <button class="ag-button"><?php echo $button_continue; ?></button>
        </div>
        <div class="clear"></div>
      </div>              
    </form>
      <!-- menu panel -->
      <?php echo $column_right; ?>
      <div class="clear"></div>   
    </div>    
  </section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>