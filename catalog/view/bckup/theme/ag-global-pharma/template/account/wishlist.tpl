<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
      <?php else: ?>
        <?php echo $breadcrumb['text']; ?>
      <?php endif; ?>
        <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel full-height wishlist-full-panel">

          <div class="col-sm-8 wishlist wishlist-box">
            
            <!-- wishlist -->
            <div class="box-panel ">
              <p class="title-content"><?php echo $heading_title; ?></p>
              <div class="para-content padding-0px">
                <?php if ($products) { ?>
                <table class="tbl-wish wish-table">
                  <thead>
                    <tr>
                      <th class="height-30px font-15 padding-left-15 padding-top-10 padding-bottom-10">Product Name</th>
                      <!-- <th><p>Product Code</p></th> -->
                      <th><p class="f-right"><?php echo $column_stock; ?></p></th>
                      <th><p class="f-right"><?php echo $column_price; ?></p></th>
                      <th><p class="f-right"><?php echo $column_action; ?></p></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($products as $product) { ?>
                    <tr>
                      <td class="height-45px font-15">
                        <p><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></p>
                      </td>
                      <!-- <td><p>AGPharma101</p></td> -->
                      <td><p class="f-right"><?php echo $product['stock']; ?></p></td>
                      <td><p class="f-right"><?php if ($product['price']) { ?>
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <b><?php echo $product['special']; ?></b> <s><?php echo $product['price']; ?></s>
                <?php } ?>
              <?php } ?></p></td>
                      <td><div class="f-right button-inside">
                          <button class="ag-button padding-5px height-25px button-cart" q="1" pid="<?php echo $product['product_id']; ?>">  
                            <i class="fa fa-shopping-cart font-18 display-block"></i>
                          </button>
                          <button class="ag-button padding-5px height-25px" onclick="window.location.href='<?php echo $product['remove']; ?>';">
                            <i class="fa fa-times font-19"></i>
                          </button>
                        </div>
                        <div class="clear"></div>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                <?php } else { ?>
                <div style="padding:10px;"><?php echo $text_empty; ?></div>
                <?php } ?>
              </div>
              
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>      
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>