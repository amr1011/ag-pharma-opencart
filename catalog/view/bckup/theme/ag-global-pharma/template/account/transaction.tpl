<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/transaction.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="bg-white" id="first-panel">
        <!-- breadcrumbs  -->
        <div class="heading-panel">
          <?php $bcount = count($breadcrumbs); ?>
          <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
          <?php if($bkey > 0): ?>
          <h2><?php echo $breadcrumb['text']; ?></h2>
          <?php if($bcount != ($bkey+1)): ?>
            <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
          <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
          <?php } ?>
        </div>


        <!-- white panel -->
        <div class="full-panel full-height history-panel">

          <!-- table panel  -->
          <div class="order-history">     
            <div class="box-panel">
              <div class="title-content-item">
                <p><?php echo $heading_title; ?></p>
                <p><?php echo $text_total; ?> <span><?php echo $total; ?></span></p>
              </div>        
              <div class="para-content padding-0px">
                <table class="tbl-wish tbl-order">
                  <thead>
                    <tr>
                      <th><?php echo $column_date_added; ?></th>
                      <th><?php echo $column_description; ?></th>
                      <th><?php echo $column_amount; ?></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($transactions) { ?>
                    <?php foreach ($transactions  as $transaction) { ?>
                    <tr>
                      <td><?php echo $transaction['date_added']; ?></td>
                      <td><?php echo $transaction['description']; ?></td>
                      <td><?php echo $transaction['amount']; ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr><td colspan="5" style="text-align:center;"><?php echo $text_empty; ?></td></tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <p class="f-right margin-top-10"><?php echo $results; ?></p>
<!--               <div class="f-left margin-top-10">
                <ul class="nav-bottom">
                  <li>1</li>
                  <li>2</li>
                  <li><i class="fa fa-caret-right font-18"></i></li>
                  <li><i class="fa fa-step-forward"></i></li>             
                </ul>
              </div> -->
              <?php echo $pagination; ?>
              <div class="clear"></div>
              
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button position-rel z-index1" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>