<div class="margin-top-20">
  <h4><?php echo $text_credit_card; ?></h4>
  <hr class="margin-bottom-20"/>
  <form class="form-horizontal">
  <table class="form-table" id="payment">
    <tbody>
      <tr>
        <td>
          <label><span class="red-color font-20">*</span> <?php echo $entry_cc_type; ?>:</label>
        </td>
        <td>
            <select class="form-control" name="cc_type" id="input-cc-type">
              <?php foreach ($cards as $card) { ?>
              <option value="<?php echo $card['value']; ?>"><?php echo $card['text']; ?></option>
              <?php } ?>
            </select>
        </td>
      </tr>
      <tr>
        <td>
          <label><span class="red-color font-20">*</span> <?php echo $entry_cc_number; ?>:</label>
        </td>
        <td>
          <input name="cc_number" value="" id="input-cc-number" type="text" class="margin-left-10 width-100per" style="margin-left:0 !important;width:100% !important;" />
        </td>
      </tr>
      <tr>
        <td>
          <label><?php echo $entry_cc_start_date; ?></label>
        </td>
        <td class="padding-top-10 padding-bottom-10">
          <div style="width:49%;margin-right:8px;" class="f-left">
            <select class="form-control" name="cc_start_date_month" id="input-cc-start-date">
              <?php foreach ($months as $month) { ?>
              <option value="<?php echo $month['value']; ?>"><?php echo $month['text']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div style="width:50%;" class="f-left">
            <select class="form-control" name="cc_start_date_year">
              <?php foreach ($year_valid as $year) { ?>
              <option value="<?php echo $year['value']; ?>"><?php echo $year['text']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="clear"></div>
        </td>
      </tr>
      <tr>
        <td>
          <label><span class="red-color font-20">*</span><?php echo $entry_cc_expire_date; ?></label>
        </td>
        <td>
          <div style="width:49%;margin-right:8px;" class="f-left">
            <select class="form-control" name="cc_expire_date_month" id="input-cc-expire-date">
              <?php foreach ($months as $month) { ?>
              <option value="<?php echo $month['value']; ?>"><?php echo $month['text']; ?></option>
              <?php } ?>
            </select>
          </div>

          <div style="width:50%;" class="f-left">
            <select class="form-control" name="cc_expire_date_year">
              <?php foreach ($year_expire as $year) { ?>
              <option value="<?php echo $year['value']; ?>"><?php echo $year['text']; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="clear"></div>
        </td>
      </tr>
      <tr>
        <td>
          <label><span class="red-color font-20">*</span> <?php echo $entry_cc_cvv2; ?></label>
        </td>
        <td>
          <input type="text" class="margin-left-10 width-100per" style="margin-left:0 !important;width:100% !important;" name="cc_cvv2" value="" id="input-cc-cvv2" />
        </td>
      </tr>
      <tr>
        <td>
          <label><?php echo $entry_cc_issue; ?></label>
        </td>
        <td>
          <input type="text" class="margin-left-10 width-100per" style="margin-left:0 !important;width:100% !important;" name="cc_issue" value="" id="input-cc-issue" />
        </td>
      </tr>

    </tbody>
  </table>
  </form>
</div>

<button class="margin-top-10  f-right ag-button" id="button-confirm" data-loading-text="<?php echo $text_loading; ?>"><strong><?php echo $button_confirm; ?></strong></button>
<script type="text/javascript"><!--
$('#button-confirm').bind('click', function() {
  $.ajax({
    url: 'index.php?route=payment/pp_pro/send',
    type: 'post',
    data: $('#payment :input'),
    dataType: 'json',
    beforeSend: function() {
      $('#button-confirm').attr('disabled', true);
      $('#payment').before('<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_wait; ?></div>');
    },
    complete: function() {
      $('.alert').remove();
      $('#button-confirm').attr('disabled', false);
    },
    success: function(json) {
      if (json['error']) {
        alert(json['error']);
      }
    
      if (json['success']) {
        location = json['success'];
      }
    }
  });
});
//--></script>