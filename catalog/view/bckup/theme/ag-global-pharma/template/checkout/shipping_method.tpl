<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p class="title-body"><?php echo $text_shipping_method; ?></p>
<?php foreach ($shipping_methods as $shipping_method) { ?>
<p class="margin-top-20 margin-left-25"><strong><?php echo $shipping_method['title']; ?></strong></p>
<?php if (!$shipping_method['error']) { ?>
<?php foreach ($shipping_method['quote'] as $quote) { ?>
<?php if ($quote['code'] == $code || !$code) { ?>
<?php $code = $quote['code']; ?>
<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" class="margin-left-25 margin-top-10 width-20px" />
<?php } else { ?>
<input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" class="margin-left-25 margin-top-10 width-20px" />
<?php } ?>
<label class="margin-left-10" for="flat-rate"><?php echo $quote['title']; ?> - <?php echo $quote['text']; ?></label>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
<?php } ?>
<?php } ?>
<?php } ?>
<p class="margin-left-25 margin-top-10"><strong><?php echo $text_comments; ?></strong></p><br/>
<textarea row="5" class="width-80per margin-left-25" name="comment"><?php echo $comment; ?></textarea><br/>

<button class="margin-top-10 f-right ag-button" id="button-shipping-method" data-loading-text="<?php echo $text_loading; ?>" style="margin:10px 20px 0px 0px;"><strong><?php echo $button_continue; ?></strong></button>