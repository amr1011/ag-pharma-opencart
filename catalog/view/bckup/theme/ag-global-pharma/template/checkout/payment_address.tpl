<div class="panel-body">
  <?php if ($addresses) { ?>
    <input id="exiting-address" name="payment_address" type="radio" class="margin-bottom-20 width-20px" checked="checked" value="existing">
    <label for="exiting-address"><?php echo $text_address_existing; ?></label>
    <div class="margin-left-20 margin-right-20">
      <div class="select margin-top-5 width-100percent">
        <select name="address_id" class="form-control">
          <?php foreach ($addresses as $address) { ?>
          <?php if ($address['address_id'] == $address_id) { ?>
          <option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
          <?php } else { ?>
          <option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
          <?php } ?>
          <?php } ?>
        </select>
      </div>
  </div>
    <input id="new-address" type="radio" name="payment_address" class="margin-bottom-20 margin-top-20 width-20px" value="new">
    <label for="new-address"><?php echo $text_address_new; ?></label>
  <?php } ?>
  <div class="margin-left-20 margin-right-20" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;" id="payment-new">
    <table class="form-table">
      <tbody>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_firstname; ?></label></td>
          <td><input type="text" name="firstname" id="input-payment-firstname" value=""></td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_lastname; ?></label></td>
          <td><input type="text" name="lastname" value="" id="input-payment-lastname"></td>
        </tr>
        <tr>
          <td><label><?php echo $entry_company; ?></label></td>
          <td><input type="text" name="company" value="" id="input-payment-company"></td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_address_1; ?></label></td>
          <td><input type="text" name="address_1" value="" id="input-payment-address-1"></td>
        </tr>
        <tr>
          <td><label><?php echo $entry_address_2; ?></label></td>
          <td><input type="text" name="address_2" value="" id="input-payment-address-2"></td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_city; ?></label></td>
          <td><input type="text" name="city" value="" id="input-payment-city"></td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_postcode; ?></label></td>
          <td><input type="text" name="postcode" value="" id="input-payment-postcode"></td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_country; ?></label></td>
          <td>
            <select name="country_id" id="input-payment-country" class="form-control" style="width:99%;">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><label><span class="red-color font-20">*</span><?php echo $entry_zone; ?></label></td>
          <td>
            <select name="zone_id" id="input-payment-zone" class="form-control" style="width:99%;"></select>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

  <button style="margin:10px 20px 0px 0px;" class="f-right ag-button" id="button-payment-address" data-loading-text="<?php echo $text_loading; ?>"><strong><?php echo $button_continue; ?></strong></button>
</div>
<script type="text/javascript"><!--
$('input[name=\'payment_address\']').on('change', function() {
  if (this.value == 'new') {
    $('#payment-existing').hide();
    $('#payment-new').show();
  } else {
    $('#payment-existing').show();
    $('#payment-new').hide();
  }
});
//--></script>
<script type="text/javascript"><!--
// Sort the custom fields
$('#collapse-payment-address .form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('#collapse-payment-address .form-group').length) {
    $('#collapse-payment-address .form-group:first').before(this);
  }
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address button[id^=\'button-payment-custom-field\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $(node).parent().find('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.time').datetimepicker({
  pickDate: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('#collapse-payment-address select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('#collapse-payment-address select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('#collapse-payment-address input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('#collapse-payment-address input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value=""><?php echo $text_select; ?></option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('#collapse-payment-address select[name=\'zone_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('#collapse-payment-address select[name=\'country_id\']').trigger('change');
//--></script>