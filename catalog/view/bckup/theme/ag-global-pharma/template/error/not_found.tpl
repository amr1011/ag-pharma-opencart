<?php echo $header; ?>
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/inner-product.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/unique-widgets.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/media.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/accordion.css">
<?php echo $content_top; ?>
<?php echo $column_left; ?>
<section class="reglar-panel  padding-bottom-50 empty-shoppping" id="first-panel">
  <div class="heading-panel">
    <?php $bcount = count($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
    <?php if($bkey > 0): ?>
    <h2><?php echo $breadcrumb['text']; ?></h2>
    <?php if($bcount != ($bkey+1)): ?>
      <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
    <?php endif; ?>
  <?php else: ?>
    <?php echo $breadcrumb['text']; ?>
  <?php endif; ?>
    <?php } ?>
  </div>

  <!-- white panel -->
  <div class="panel-group margin-top-50 empty-data">
    <h3 class="font-30"><?php echo $heading_title; ?></h3>
    <p><?php echo $text_error; ?></p>
  </div>
  <div class="panel-group padding-20px">
    <div class="margin-top-20">       
      <button class="ag-button f-right" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
      <div class="clear"></div>
    </div>

  </div>    
</section>
<?php echo $column_right; ?>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>