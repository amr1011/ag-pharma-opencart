<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/ty-sent.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
        <?php else: ?>
          <?php echo $breadcrumb['text']; ?>
        <?php endif; ?>
        <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel full-height ty-full-list">

          <div class="col-sm-8 ty-panel">
            <p class="font-30 margin-bottom-15"><?php echo $heading_title; ?></p>
            <!-- wishlist -->
            <p class="font-15 font-bold"><?php echo $text_message; ?></p>

            <div class="f-right margin-top-20 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>