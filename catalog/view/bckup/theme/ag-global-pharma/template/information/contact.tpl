<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/contact.css">
  <section class="contact-us-panel" id="first-panel">
    <div class="contact-us-panel-center">
      <div class="contact-us-panel-content">
        <h2 class="heading-text dark-green margin-bottom-30">Contact Us</h2>
        <p class="margin-bottom-30 font-15">For questions, comments and suggestions, get in touch with us today. Fill out the form below and one of our representatives will get back to you.</p>
        
        <div class="form-container">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <input type="text" placeholder="<?php echo $entry_name; ?>" required class="contact-name" name="name" value="<?php echo $name; ?>" />
            <input type="email" placeholder="<?php echo $entry_email; ?>" required class="contact-email" name="email" value="<?php echo $email; ?>" />
            <input type="text" placeholder="Subject" required class="contact-subject" name="subject" value="" />
            <textarea name="enquiry"><?php echo $enquiry; ?></textarea>

            <button class="ag-button">Send</button>
          </form>
        </div>

        <div class="clear"></div>

        <div class="contact-text">
          <p class="address">2nd Floor, NVM Mall, Sayre Highway, Valencia City, Bukidnon, Philippines</p>
          <p class="telephone">Telefax: (088) 828.5513</p>
          <p class="mobile">Mobile: 0917.309.0915</p>
        </div>
      </div> <!-- first-panel-content -->
    </div> <!-- first-panel-center -->
  </section> <!-- first-panel -->
  <div id="map" class="contact-us-map"></div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="catalog/view/theme/ag-global-pharma/js/maps.js"></script>
<?php echo $footer; ?>