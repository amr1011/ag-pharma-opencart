<?php echo $header; ?>
<?php echo $content_top; ?>
	<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/products.css">
	<section class="p-first-panel" id="first-panel">
		<div class="p-panel-content">
			<h2 class="heading-text white-color">Our Products</h2>
		</div>
	</section>
	<?php $i = 0; foreach($products as $product) { ?>
<!-- 		<section class="product-<?php echo strtolower(str_replace(' ', '-', $product['name'])); ?>">		
			<div class="content-<?php echo ($i%2 == 0) ? 'odd' : 'even'; ?>">
				<img src="<?php echo $product['image']; ?>" alt="">
				<p><?php echo html_entity_decode($product['attributes']['description']['text']); ?></p>
				<a href="#" class="ag-button button-first button-cart" pid="<?php echo $product['product_id']; ?>" q="1">Add To Cart</a>
				<a href="<?php echo $product['link']; ?>" class="ag-button">Read More</a>
			</div>
		</section>

		<div class="clear"></div> -->
		<section class="product-<?php echo strtolower(str_replace(' ', '-', $product['name'])); ?> product-<?php echo ($i%2 == 0) ? 'odd' : 'even'; ?>">
			<div class="content-<?php echo ($i%2 == 0) ? 'odd' : 'even'; ?>">
				<img src="<?php echo $product['image']; ?>" alt="">
				<p><?php echo html_entity_decode($product['attributes']['description']['text']); ?></p>
				<a href="#" class="ag-button button-first button-cart" pid="<?php echo $product['product_id']; ?>" q="1">Add To Cart</a>
				<a href="<?php echo $product['link']; ?>" class="ag-button">Read More</a>
			</div>
		</section>

		<div class="clear"></div>
	<?php $i++; } ?>
<?php echo $footer; ?>