<?php echo $header; ?>
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/inner-product.css">
<?php echo $content_top; ?>
<?php $class_name = str_replace(' ','-',strtolower($heading_title)); ?> 
  <section class="<?php echo $class_name; ?>-panel" id="first-panel">
    <div class="heading-panel">
      <i class="fa fa-home"></i>
      <h2><?php echo $heading_title; ?></h2>
    </div>

    <div class="left-panel">
      <div class="panel-banner <?php echo $class_name; ?>-banner">
        <div class="panel-banner-text">
          <img src="<?php echo $orig_image; ?>" alt="">
          <p><?php echo html_entity_decode($attributes['description']['text']); ?></p>
        </div>

        <div class="clear"></div>
      </div>

      <div class="left-panel-content <?php echo $class_name; ?>-content"><?php echo $description; ?></div> <!-- left-panel-content -->
    </div> <!-- left-panel -->

    <div class="right-panel">
      <div class="nav-show-button">
        <button class="ag-button width-60px"><i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></button>
      </div>
      
      <div class="right-panel-content">
        <?php if ($manufacturer) { ?>
        <p><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></p>
        <?php } ?>
        <?php echo $text_model; ?> <?php echo $model; ?>
        <?php if ($reward) { ?>
        <p><?php echo $text_reward; ?> <?php echo $reward; ?></p>
        <?php } ?>
        <p><?php echo $text_stock; ?> <?php echo $stock; ?></p>
        <!-- <p>Item: Per Box</p> -->
        <?php if ($price) { ?>
        <?php if (!$special) { ?>
        <h3 class="item-price"><?php echo $price; ?></h3>
        <?php } else { ?>
        <h3 class="item-price"><?php echo $price; ?></h3>
        <p><?php echo $special; ?></p>
        <?php } ?>
        <?php if ($tax) { ?>
        <p><?php echo $text_tax; ?> <?php echo $tax; ?></p>
        <?php } ?>
        <?php if ($points) { ?>
        <p><?php echo $text_points; ?> <?php echo $points; ?></p>
        <?php } ?>
        <?php if ($discounts) { ?>
        <p>
          <hr>
        </p>
        <?php foreach ($discounts as $discount) { ?>
        <p><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></p>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <form action="">
          <label for="">Quantity</label>
          <input type="text" class="width-145px" name="quantity" value="">
          <input type="hidden" name="pid" value="<?php echo $product_id; ?>">
          <button href="#" class="add-button  ag-button width-30px">+</button>
          <button href="#" class="minus-button  ag-button width-30px">-</button>
          <button href="#" class="add-to-button ag-button width-145px padding-top-12 valign-top"><i class="fa fa-shopping-cart font-20 margin-right-5"></i>Add to Cart</button>
          <button pid="<?php echo $product_id; ?>" href="#" class="add_wishlist add-to-button ag-button padding-top-12 width-63px valign-top padding-bottom-13 padding-top-17 line-height-0"><i class="fa fa-heart font-20"></i></button>
        </form>
      </div> <!-- right-panel-content -->
    </div> <!-- right-panel -->

    <div class="clear"></div>
  </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>