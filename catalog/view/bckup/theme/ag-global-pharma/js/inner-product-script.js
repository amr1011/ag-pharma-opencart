$(document).ready(function(){
	$('.nav-show-button .ag-button').click(function(){
		$('.right-panel').toggleClass('addcart-show');
		$('.nav-show-button .fa').toggleClass('show-button-rotate');
	});

	$('.add-button, .minus-button').click(function(e) {
		var add = ($(this).is('.add-button')) ? true : false;
		var quantity = ($('input[name=quantity]').length > 0) ? parseInt($('input[name=quantity]').val()) : $(this).closest('td').find('input[type=text]').val();

		if(isNaN(quantity)) {
			quantity = 0;
		}

		if(add) {
			if(quantity < 0) {
				quantity = 1;
			} else {
				quantity++;
			}
		} else {
			if(quantity < 0) {
				quantity = 0;
			} else if(quantity > 0) {
				quantity--;
			}
		}

		if($('input[name=quantity]').length > 0) {
			$('input[name=quantity]').val(quantity);
		} else {
			$(this).closest('td').find('input[type=text]').val(quantity);
		}
		e.preventDefault();
	});

	$('.remove-to-cart').click(function(e) {
		var key = $(this).attr('cid');

		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					window.location.href = 'index.php?route=checkout/cart';
				}, 100);

/*				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}*/
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});

		e.preventDefault();
	});
});