$(document).ready(function(){
	$('a.nav-icon').click(function(e){
		$('ul.child-nav').toggleClass('nav-show');
		$('a.nav-icon').toggleClass('nav-padding-bot');
		$('a.nav-icon').toggleClass('active-nav');
		$('a.nav-icon > .bar.top-bar').toggleClass('transform-top');
		$('a.nav-icon > .bar.bot-bar').toggleClass('transform-bot');
		$('a.nav-icon > .bar.mid-bar').toggleClass('hide-mid');
		e.preventDefault();
	});																																																																																		

	// smooth scroll
    $(function() {
      $('.back-to-top a[href*=#]:not([href=#])').click(function() {
/*        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 2000);
            return false;
          }
        }*/
        $('html,body').animate({
          scrollTop: $('#first-panel').offset().top
        }, 2000);
        return false;
      });
    });


    // ONCLICK NAVIGATION 
    // resposive menu     
/*    $(".white-menu .nav-response .fa-arrow-left").click(function() {    

    	// navigation on right side 
    	$(".login-nav").toggleClass("login-move-left");
    	$(".regs-nav").toggleClass("regs-move-left");
        $(".forgot-nav").toggleClass("forgot-move-left");
    	$(".wish-nav").toggleClass("wish-move-left");
        $(".order-menu").toggleClass("order-move-left");

    	var browserWidth;
    	browserWidth = $(window).width();

    	if (browserWidth > 768) {
    		$(".open-menu").toggleClass("compact-menu"); 	
    		$(".regs-panel1, .regs-panel2").toggleClass("compact-menu1");
    		$(".forgot-panel").toggleClass("compact-menu2");
    		$(".wishlist").toggleClass("compact-menu2");
    		$(".full-panel .login-panel1").toggleClass("compact-menu");  
            $(".order-history").toggleClass("order-history-left");
            $(".history-content").toggleClass("history-move-left");

    	}
    	// get width of the screen
    	var myWidth;
    	myWidth = $(window).width();

    	if (myWidth < 992) {
    		if ($(".full-panel .white-menu").hasClass("move-left")) {    			
    			$(".wishlist table, .wishlist table p").css({
    				// 'text-align':'center',    				   		
    			});    			    		
    			$(".wishlist table td").css({
    				'padding':'5px 10px 5px 5px'
    			});
    		} else {
    			$(".wishlist table").css({
    				// 'text-align':'left',    				   		
    			});    			
    		}
    	}
    	if ($(".white-menu .nav-response .fa").hasClass("fa-arrow-left")) {    		
    		$(".white-menu .nav-response .fa").removeClass("fa-arrow-left").addClass("fa-arrow-right");
    	} else {    		
    		$(".white-menu .nav-response .fa").removeClass("fa-arrow-right").addClass("fa-arrow-left");
    	}
    });*/    


    $(".white-menu .nav-response .fa-arrow-left").click(function() {    
        
        $(".login-navigation").toggleClass("login-move-left");
        
        // navigation on right side 
        $(".transaction-menu").toggleClass("transaction-move-left");
        $(".product-return-nav").toggleClass("return-move-left");
        $(".registration-menu").toggleClass("regs-move-left");
        $(".forgot-navigation").toggleClass("forgot-move-left");
        $(".wish-navigation").toggleClass("wish-move-left");
        $(".order-history-nav").toggleClass("order-move-left");
        $(".history-menu").toggleClass("history-move-left");
        $(".wish-navigation").toggleClass("account-edit-move-left");
        $(".account-user-navigation").toggleClass("user-move-left");
        $(".change-password").toggleClass("change-move-left");
        $(".edit-navigation").toggleClass("edit-move-left");
        $(".address-navigation").toggleClass("addressbook-move-left");
        $(".return-navigation").toggleClass("return-move-left");
        $(".form-navigation").toggleClass("form-move-left");
        $(".news-navigation").toggleClass("news-move-left");
        $(".ty-navigation").toggleClass("ty-move-left");
        
        var browserWidth;
        browserWidth = $(window).width();

        if (browserWidth > 768) {
            // $(".open-menu").toggleClass("compact-menu");     
            // $(".regs-panel1, .regs-panel2").toggleClass("compact-menu1");
            // $(".forgot-panel").toggleClass("compact-menu2");
            // $(".wishlist").toggleClass("compact-menu2");
            // $(".full-panel .login-panel1").toggleClass("compact-menu");  
            // $(".order-history-nav").toggleClass("order-history-left");
            // $(".history-content").toggleClass("history-move-left");

        }
        // get width of the screen
        var myWidth;
        myWidth = $(window).width();

        if (myWidth < 992) {
            if ($(".full-panel .white-menu").hasClass("move-left")) {               
                $(".wishlist table, .wishlist table p").css({
                    // 'text-align':'center',                           
                });                         
                $(".wishlist table td").css({
                    'padding':'5px 10px 5px 5px'
                });
            } else {
                $(".wishlist table").css({
                    // 'text-align':'left',                         
                });             
            }
        }
        if ($(".white-menu .nav-response .fa").hasClass("fa-arrow-left")) {         
            $(".white-menu .nav-response .fa").removeClass("fa-arrow-left").addClass("fa-arrow-right");
        } else {            
            $(".white-menu .nav-response .fa").removeClass("fa-arrow-right").addClass("fa-arrow-left");
        }
    });

    // check for width 
    var panelWidth;   
    var firstWidth, secondWidth;

    $(window).resize(function() {
    	panelWidth = $(window).width();  

    // 	// check if 768
    	if (panelWidth = 768) {    	    		

    		firstWidth = $(".full-panel .login-panel1").width();
    		secondWidth = $(".full-panel .login-panel2").width();   
    		// alert(secondWidth);

    		// $(".full-panel .login-panel1").css({'width':secondWidth});
    		if (firstWidth = "642") {
    			// $(".full-panel .login-panel1").css({'width':'60%'});
    		} else {
    			// $(".full-panel .login-panel2").css({'width':'33.3%'});
    		}

    	}
    	else {
    		// $(".full-panel .box-panel:nth-of-type(1)").css({'width':'33.3%'});
    	}    	
    })


	$('.button-cart, .add-to-button').on('click', function(e) {
        if($(this).is(':not(.add_wishlist)')) {
            var product_id = ($(this).is('.add-to-button')) ? $('input[name=pid]').val() : $(this).attr('pid');
            var quantity = ($(this).is('.add-to-button')) ? parseInt($('input[name=quantity]').val()) : $(this).attr('q');
            var t = $(this);

            $.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                //data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                data: {
                    product_id: product_id,
                    quantity: quantity
                },
                dataType: 'json',
                beforeSend: function() {
                    t.button('loading');
                },
                complete: function() {
                    t.button('reset');
                },
                success: function(json) {
    /*              var url = t.closest('div').find('.ag-button:eq(1)').attr('href');

                    if(url != undefined) {
                        url = url.replace(/[^\/]+$/i, '');
                        window.location.href = url+'index.php?route=checkout/cart';
                    } else {
                        window.location.href = 'index.php?route=checkout/cart';
                    }*/
                    
                    window.location.href = 'index.php?route=checkout/cart';
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }

		e.preventDefault();
	});

    $('.add_wishlist').click(function(e) {
        var product_id = $(this).attr('pid');

        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function(json) {
                $('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    //$('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    window.location.href = 'index.php?route=account/wishlist';
                }

/*                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');*/
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

        e.preventDefault();
    });
});