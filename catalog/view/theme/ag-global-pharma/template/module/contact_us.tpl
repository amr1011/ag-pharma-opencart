<section class="contact-panel">
    <div id="map"></div> <!-- map -->

    <div class="contact-form">
        <div class="contact-centered">
            <div class="contact-content">
                <h2 class="heading-text margin-bottom-20 white-color">Contact Us</h2>
                <p class="margin-bottom-30">We’d like to hear from you. Contact us today by filling out the form below and we’ll get back to you as soon as possible.</p>


                <ul class="contact-info">
                    <li>
                        <i class="fa fa-map-marker"></i>
                        <p>2nd floor, NVM Mall, Sayre Highway, Valencia City, Bukidnon Philippines</p>
                        <div class="clear"></div>
                    </li>

                    <li>
                        <i class="fa fa-phone"></i>
                        <p>Telefax: (088) 828.5513</p>
                        <div class="clear"></div>
                    </li>
                    
                    <li>
                        <i class="fa fa-mobile"></i>
                        <p>Mobile: 0917.309.0915</p>
                        <div class="clear"></div>
                    </li>
                </ul>

                <form>
                    <input class="form-name" type="text" name="name" placeholder="Name(Required)" required>
                    <input class="form-email" type="email" name="email" placeholder="Email(Required)" required>
                    <input class="form-contact" type="text" name="number" placeholder="Contact No.(Required)" required>
                    <textarea placeholder="Message(Required)" required></textarea>
                    <button class="send-button">Send</button>
                    <!-- <div class="g-recaptcha" data-sitekey="6LcYQw8TAAAAAADjHlVz_p5htydoRlOpsVBJZ3NA"></div> -->
                </form>

                <div class="clear"></div>
            </div> <!-- contact-content -->
        </div> <!-- contact-centered -->
    </div> <!-- contact-form -->
</section> <!-- contact-panel -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="catalog/view/theme/ag-global-pharma/js/maps.js"></script>