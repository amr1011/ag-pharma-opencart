<?php
  if(!$logged) {
    $links[$text_login] = array('link' => $login, 'icon' => 'sign-in');
    $links[$text_register] = array('link' => $register, 'icon' => 'pencil');
    $links[$text_forgotten] = array('link' => $forgotten, 'icon' => 'unlock');
  }

  if($logged) {
    $links[$text_wishlist] = array('link' => $wishlist, 'icon' => 'gift');
    $links[$text_order] = array('link' => $order, 'icon' => 'history');
    $links[$text_return] = array('link' => $return, 'icon' => 'step-backward');
    $links[$text_transaction] = array('link' => $transaction, 'icon' => 'shopping-cart');
    $links[$text_logout] = array('link' => $logout, 'icon' => 'sign-out');
  }

  $html = $html_link = '';
  $print = false;

  switch($_GET['route']) {
    case 'account/login': case 'account/login/':
      $print = true;
      $html = '<div class="nav-right position-rel ">
          <div class="white-menu col-sm-4 login-nav margin-top-30">
            <div class="nav-response bg-white">
              <i class="fa fa-arrow-left"></i>
            </div>
            {LINKS}
          </div>

          <div class="clear"></div>
        </div>';
    break;
    case 'account/register': case 'account/register/':
      $print = true;
      $html = '<div class="registration-menu nav-right">
        <div class="white-menu col-sm-4 regs-nav">
          <div class="nav-response bg-white">
            <i class="fa fa-arrow-left"></i>
          </div>
          {LINKS}
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div><div class="clear"></div>';
    break;
    case 'account/forgotten': case 'account/forgotten/':
      $print = true;
      $html = '<div class="forgot-navigation">
  <div class="white-menu col-sm-4 forgot-nav">
    <div class="nav-response bg-white">
      <i class="fa fa-arrow-left"></i>
    </div>
    {LINKS}
  </div>
</div>';
    break;
    case 'account/wishlist': case 'account/wishlist/':
      $print = true;
      $html = '  <div class="position-rel nav-right">
    <div class="white-menu col-sm-4 wish-nav">
      <div class="nav-response bg-white">
        <i class="fa fa-arrow-left"></i>
      </div>
      {LINKS}
    </div>
    <div class="clear"></div>
  </div>';
    break;
    case 'account/order': case 'account/order/':
      $print = true;
      $html = '<div class="order-history-nav">
  <div class="white-menu order-menu">
    <div class="nav-response bg-white default-cursor">
      <i class="fa fa-arrow-left"></i>
    </div>
    {LINKS}
    <div class="clear"></div>
  </div>
</div>';
    break;
    case 'account/return': case 'account/return/':
      $print = true;
      $html = '<div class="product-return-nav">
        <div class="white-menu order-menu return-menu">
          <div class="nav-response bg-white">
            <i class="fa fa-arrow-left"></i>
          </div>
          {LINKS}
        </div>
        <div class="clear"></div>
      </div>';
    break;
    case 'account/transaction': case 'account/transaction/':
      $print = true;
      $html = '      <div class="position-rel transaction-menu">
        <div class="white-menu order-menu transact-nav">
          <div class="nav-response bg-white">
            <i class="fa fa-arrow-left"></i>
          </div>
          {LINKS}
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>';
    break;
  }

  if($print) {
    foreach($links as $text => $link) { ob_start(); ?>
      <div class="control-icon">
      <a href="<?php echo $link['link']; ?>" title="<?php echo $text; ?>">
        <i class="fa fa-<?php echo $link['icon']; ?>"></i>         
        <button class="ag-button"><?php echo $text; ?></button>
      </a>
      </div>
    <?php
        $html_link .= ob_get_clean();
    }

    $html = str_replace('{LINKS}', $html_link, $html);
    echo $html;
  }
  ?>