<section class="fourth-panel">
    <div class="fourth-panel-center">
        <div class="fourth-panel-content">
            <h2 class="heading-text margin-bottom-50 orange-color"><?php echo $heading_title; ?></h2>

            <div class="testimonial-container">
                <ul class="rslides" id="slider">
                    <li>
                        <div class="testi-item">
                            <div class="testi-image"><img src="https://placeimg.com/150/150/animals" alt=""></div>  
                            
                            <div class="testi-content">
                                <h3>Yuki Saito</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, a, mollitia, dolores hic ab repellat quod eligendi sequi voluptate molestiae minus repudiandae quo asperiores veniam in quis suscipit. Odit, eveniet, ratione minima tempore .</p>
                            </div>

                        </div> <!-- testi-item -->

                        <div class="testi-item">
                            <div class="testi-image"><img src="https://placeimg.com/150/150/tech" alt=""></div> 
                            
                            <div class="testi-content">
                                <h3>Yuki Saito</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, a, mollitia, dolores hic ab repellat quod eligendi sequi voluptate molestiae minus repudiandae quo asperiores veniam in quis suscipit. Odit, eveniet, ratione minima tempore .</p>
                            </div>
                        </div> <!-- testi-item -->

                        <div class="clear"></div>
                    </li>

                    <li>
                        <div class="testi-item">
                            <div class="testi-image"><img src="https://placeimg.com/150/150/animals" alt=""></div>  
                            
                            <div class="testi-content">
                                <h3>Saito, Yuki</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, a, mollitia, dolores hic ab repat quod eligendi sequi voluptate molestiae minus repudiandae quo asperiores veniam in quis suscipit. Odit, eveniet, ratione minima tempore .</p>
                            </div>
                        </div> <!-- testi-item -->

                        <div class="testi-item">
                            <div class="testi-image"><img src="https://placeimg.com/150/150/tech" alt=""></div> 
                            
                            <div class="testi-content">
                                <h3>Angel Locsin</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, a, mollitia, dolores hic ab repellat quod eligendi sequi voluptate molestiae minus repudiandae quo asperiores veniam in quis suscipit. Odit, eveniet, ratione minima tempore .</p>
                            </div>
                        </div> <!-- testi-item -->

                        <div class="clear"></div>
                    </li> <!-- list-item -->
                </ul>
            </div> <!-- testimonial-container -->
        </div> <!-- fourth-panel-content -->
    </div> <!-- fourth-panel-center -->
</section> <!-- fourth-panel -->
<script src="catalog/view/theme/ag-global-pharma/js/responsiveslides.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#slider").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 8000,          // Integer: Time between slide transitions, in milliseconds
        pager: true,           // Boolean: Show pager, true or false
        nav: true,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "centered-btns",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });
});
</script>