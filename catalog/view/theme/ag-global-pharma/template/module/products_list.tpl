<?php if(is_array($products) && count($products) > 0) { ?>
    <section class="third-panel">
        <div class="third-panel-center">
            <div class="third-panel-content">
                <h2 class="heading-text margin-bottom-20"><?php echo $heading_title; ?></h2>
                
                <div class="product-col-container">
                    <?php foreach($products as $product) { ?>
                        <div class="product-col">
                            <img src="<?php echo $product['image']; ?>" alt="" />
                            <p><?php echo html_entity_decode($product['attributes']['description']); ?></p>
                            <div class="product-button"><a href="<?php echo $product['link']; ?>">Read More</a></div>
                        </div> <!-- product-col -->
                    <?php } ?>
                    <div class="clear"></div>
                </div>

                <div class="clear"></div>
            </div> <!-- third-panel-content -->
        </div> <!-- third-panel-center -->
    </section> <!-- third-panel -->
<?php } ?>