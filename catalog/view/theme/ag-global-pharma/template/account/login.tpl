<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/login.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
            <?php $bcount = count($breadcrumbs); ?>
            <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
            <?php if($bkey > 0): ?>
            <h2><?php echo $breadcrumb['text']; ?></h2>
            <?php if($bcount != ($bkey+1)): ?>
              <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
            <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
            <?php } ?>
        </div>

        <!-- white panel -->
        <div class="full-panel login-full-height">

          <div class="cont-panel position-rel">
            <!-- first panel -->

            <!-- <div class="div-inside"> -->
              <div class="box-panel display-inline-top col-sm-4 margin-top-30 login-panel1">
                <p class="title-content"><?php echo $text_new_customer; ?></p>
                <div class="para-content ">
                  <p class="para-title"><?php echo $text_register; ?></p>
                  <p ><?php echo $text_register_account; ?></p>
                  <button class="ag-button" onclick="window.location.href='<?php echo $register; ?>';"><?php echo $button_continue; ?></button>
                </div>
              </div>

              <!-- second panel -->
              <div class="box-panel open-menu display-inline-top col-sm-4 margin-top-30 login-panel2">
                <p class="title-content"><?php echo $text_returning_customer; ?></p>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                  <div class="para-content">
                    <?php if ($success) { ?>
                    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
                    <?php } ?>
                    <?php if ($error_warning) { ?>
                    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
                    <?php } ?>
                    <p class="para-title"><?php echo $text_i_am_returning_customer; ?></p>
                    <p class="font-15"><?php echo $entry_email; ?></p>
                    <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" />
                    
                    <p class="margin-top-10 font-15"><?php echo $entry_password; ?></p>
                    <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" />
                    <br />
                    <a href="#" class="font-12"><?php echo $text_forgotten; ?></a>
                    <br />
                    <a href="#">
                      <button class="ag-button margin-top-20"><?php echo $button_login; ?></button>
                    </a>
                    <?php if ($redirect) { ?>
                    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                    <?php } ?>
                  </div>
                </form>
              </div>

              <div class="clear"></div>
            <!-- </div> -->

          </div>
          <!-- menu panel -->
           <?php echo $column_right; ?>

          <div class="clear"></div>
        </div>  
        <div class="clear"></div> 
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>