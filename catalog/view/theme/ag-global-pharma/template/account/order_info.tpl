<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/history-view.css">
<?php echo $content_top; ?>
<?php echo $column_left; ?>
  <section class="bg-white" id="first-panel">
    <!-- breadcrumbs  -->
    <div class="heading-panel">
    <?php $bcount = count($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
    <?php if($bkey > 0): ?>
    <h2><?php echo $breadcrumb['text']; ?></h2>
    <?php if($bcount != ($bkey+1)): ?>
      <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
    <?php endif; ?>
  <?php else: ?>
    <?php echo $breadcrumb['text']; ?>
  <?php endif; ?>
    <?php } ?>
    </div>


    <!-- white panel -->
    <div class="full-panel fixed-page-height padding-40px history-panel margin-top-30">

      <div class="position-rel f-left history-content">

        <!-- order information  -->
        <div class="">      
          <div class="">
            <div class="title-content-item">
              <p><?php echo $heading_title; ?></p>            
            </div>        
            <div class="para-content padding-0px">
              <table class="tbl-order1">
                <thead>
                  <tr>
                    <th colspan="2"><?php echo $text_order_detail; ?></th>          
                  </tr>
                </thead>
                <tbody>                                   
                  <tr>
                    <td class="width-50percent"><?php if ($invoice_no) { ?>
                      <p class="text-left"><span class="font-bold black-color"><?php echo $text_invoice_no; ?></span> <?php echo $invoice_no; ?></p>
                      <?php } ?>
                      <p class="text-left"><span class="font-bold black-color"><?php echo $text_order_id; ?></span> #<?php echo $order_id; ?></p>
                      <p class="text-left"><span class="font-bold black-color"><?php echo $text_date_added; ?></span> <?php echo $date_added; ?></p>
                    </td>                 
                    <td><?php if ($payment_method) { ?>
                      <p class="f-left"><span class="font-bold black-color"><?php echo $text_payment_method; ?></span> <?php echo $payment_method; ?></p>
                      <?php } ?>
                      <?php if ($shipping_method) { ?><br />
                      <p class="f-left"><span class="font-bold black-color"><?php echo $text_shipping_method; ?></span> <?php echo $shipping_method; ?>
                      <?php } ?>
                    </td>                                   
                  </tr>                               
                </tbody>
              </table>
            </div>          
          </div>          
        </div>  

        <!-- payment address  -->
        <div class="payment-address margin-top-30">
          <p><?php echo $text_payment_address; ?></p>

          <div><?php echo $payment_address; ?></div>
        </div>

        <?php if ($shipping_address) { ?>
        <div class="payment-address margin-top-30">
          <p><?php echo $text_shipping_address; ?></p>

          <div><?php echo $shipping_address; ?></div>
        </div>
        <?php } ?>

        <!-- for weird table  -->
        <div class="weird-table margin-top-30">
          <table>
            <thead>
              <tr>
                <th><?php echo $column_name; ?></th>
                <!-- <th><?php echo $column_model; ?></th> -->
                <th><?php echo $column_quantity; ?></th>
                <th><?php echo $column_price; ?></th>
                <th><?php echo $column_total; ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td><?php echo $product['name']; ?><?php foreach ($product['option'] as $option) { ?>
                <br />
                &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                <?php } ?>
                </td>
                <!-- <td></td> -->
                <td><?php echo $product['quantity']; ?></td>
                <td><?php echo $product['price']; ?></td>
                <td><?php echo $product['total']; ?></td>
                <td>
                  <button class="ag-button" onclick="window.location.href='<?php echo $product['reorder']; ?>';" title="<?php echo $button_reorder; ?>">
                    <i class="fa fa-shopping-cart"></i>
                  </button>
                  <button class="ag-button" onclick="window.location.href='<?php echo $product['return']; ?>';" title="<?php echo $button_return; ?>">
                    <i class="fa fa-undo"></i>
                  </button>
                </td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td><?php echo $voucher['description']; ?></td>
                <!-- <td></td> -->
                <td>1</td>
                <td><?php echo $voucher['amount']; ?></td>
                <td><?php echo $voucher['amount']; ?></td>
                <td></td>
              </tr>
              <?php } ?>
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td></td>
                <td></td>
                <td><?php echo $total['title']; ?></td>
                <td><?php echo $total['text']; ?></td>
                <td></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php if ($comment) { ?>
          <table>
            <thead>
              <tr>
                <td><?php echo $text_comment; ?></td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo $comment; ?></td>
              </tr>
            </tbody>
          </table>
          <?php } ?>
        </div>

        <!-- order history  -->
        <div class="margin-top-30">
          <?php if ($histories) { ?>
            <div class="box-panel">
              <div class="title-content-item">
                <p><?php echo $text_history; ?></p>            
              </div>        
              <div class="para-content padding-0px">
                <table class="tbl-order">
                  <thead>
                    <tr>
                      <th><?php echo $column_date_added; ?></th>
                      <th><?php echo $column_status; ?></th>
                      <th><?php echo $column_comment; ?></th>                
                    </tr>
                  </thead>
                  <tbody>               
                    <?php foreach ($histories as $history) { ?> 
                    <tr>
                      <td><?php echo $history['date_added']; ?></td>
                      <td><?php echo $history['status']; ?></td>
                      <td><?php echo $history['comment']; ?></td>
                    </tr>       
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php } ?>
          <div class="f-right margin-top-10 margin-bottom-30">
            <button class="ag-button position-rel z-index1" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
          </div>
          <div class="clear"></div>
        </div>    

      </div>
      <?php echo $column_right; ?>
      <div class="clear"></div>   
    </div>  
  </section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>