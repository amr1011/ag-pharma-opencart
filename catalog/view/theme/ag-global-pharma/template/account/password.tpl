<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/change.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
    <section class="reglar-panel" id="first-panel">
      <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
        <?php else: ?>
          <?php echo $breadcrumb['text']; ?>
        <?php endif; ?>
        <?php } ?>
      </div>

      <!-- white panel -->
      <div class="full-panel full-height ">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
          <div class="col-sm-8 change-pass">
            <p class="font-30 margin-bottom-15"><?php echo $heading_title; ?></p>
            <!-- wishlist -->

            <div class="box-panel ">
              <p class="title-content"><?php echo $text_password; ?></p>
              <div class="para-content padding-20px">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-10"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_password; ?></p>
                      </td>
                      <td>
                        <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>">
                        <?php if ($error_password) { ?>
                        <div class="text-danger"><?php echo $error_password; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-10"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_confirm; ?></p>
                      </td>
                      <td>
                        <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>">
                        <?php if ($error_confirm) { ?>
                        <div class="text-danger"><?php echo $error_confirm; ?></div>
                        <?php } ?>
                      </td>
                    </tr>               
                  </tbody>
                </table>
              </div>
              
            </div>
            <div class="f-left margin-top-10 margin-bottom-30"><button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>              
        </form>
        <?php echo $column_right; ?>
        <div class="clear"></div>   
      </div>    
    </section>

    <?php echo $content_bottom; ?>
<?php echo $footer; ?>