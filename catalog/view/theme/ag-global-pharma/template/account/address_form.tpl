<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/edit-address.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="reglar-panel" id="first-panel">
        <div class="heading-panel">
          <?php $bcount = count($breadcrumbs); ?>
          <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
          <?php if($bkey > 0): ?>
          <h2><?php echo $breadcrumb['text']; ?></h2>
          <?php if($bcount != ($bkey+1)): ?>
            <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
          <?php endif; ?>
          <?php else: ?>
            <?php echo $breadcrumb['text']; ?>
          <?php endif; ?>
          <?php } ?>
        </div>



        <!-- white panel -->
        <div class="full-panel full-height edit-address-panel">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
          <div class="col-sm-8 edit-address">
            
            <!-- wishlist -->

            <div class="box-panel ">
              <p class="f-right margin-bottom-5">Fields with <span class="red-color-ag font-20 font-bold ">* </span>   are required</p>
              <div class="clear"></div>
              <p class="title-content"><?php echo $text_edit_address; ?></p>
              <div class="para-content padding-20px">
                <table class="width-100percent">
                  <tbody>
                    <tr>
                      <td class="width-200px">
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold ">* </span><?php echo $entry_firstname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>">
                        <?php if ($error_firstname) { ?>
                        <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_lastname; ?></p>
                      </td>
                      <td>
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>">
                        <?php if ($error_lastname) { ?>
                        <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">*</span> <?php echo $entry_company; ?></p>
                      </td>
                      <td>
                        <input type="text" name="company" value="<?php echo $company; ?>" placeholder="<?php echo $entry_company; ?>">
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_address_1; ?></p>
                      </td>
                      <td>
                        <input type="text" name="address_1" value="<?php echo $address_1; ?>" placeholder="<?php echo $entry_address_1; ?>">
                        <?php if ($error_address_1) { ?>
                        <div class="text-danger"><?php echo $error_address_1; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_address_2; ?></p>
                      </td>
                      <td>
                        <input type="text" name="address_2" value="<?php echo $address_2; ?>" placeholder="<?php echo $entry_address_2; ?>">
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_city; ?></p>
                      </td>
                      <td>
                        <input type="text" name="city" value="<?php echo $city; ?>" placeholder="<?php echo $entry_city; ?>">
                        <?php if ($error_city) { ?>
                        <div class="text-danger"><?php echo $error_city; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_postcode; ?></p>
                      </td>
                      <td>
                        <input type="text" name="postcode" value="<?php echo $postcode; ?>" placeholder="<?php echo $entry_postcode; ?>">
                        <?php if ($error_postcode) { ?>
                        <div class="text-danger"><?php echo $error_postcode; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_country; ?></p>
                      </td>
                      <td>
                        <div class="select margin-top-5 width-100percent">
                          <select name="country_id" id="input-country" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($countries as $country) { ?>
                            <?php if ($country['country_id'] == $country_id) { ?>
                            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                          </select>
                        </div>
                        <?php if ($error_country) { ?>
                        <div class="text-danger"><?php echo $error_country; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><span class="red-color-ag font-20 font-bold">* </span><?php echo $entry_zone; ?></p>
                      </td>
                      <td>
                        <div class="select margin-top-5 width-100percent">
                          <select name="zone_id" id="input-zone" class="form-control"></select>
                        </div>
                        <?php if ($error_zone) { ?>
                        <div class="text-danger"><?php echo $error_zone; ?></div>
                        <?php } ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p class="font-15 font-lato f-right margin-right-20 margin-top-5"><?php echo $entry_default; ?></p>
                      </td>
                      <td>
                        <?php if ($default) { ?>
                          <div class="display-inline-mid">
                            <input type="radio" name="default" id="yes" class="display-inline-mid width-20px" checked="checked">
                            <label for="yes" class="display-inline-mid margin-top-10 default-cursor"><?php echo $text_yes; ?></label>
                          </div>
                          <div class="display-inline-mid">
                            <input type="radio" name="default" id="no" class="display-inline-mid width-20px">
                            <label for="no" class="display-inline-mid margin-top-10 default-cursor"><?php echo $text_no; ?></label>
                          </div>
                        <?php } else { ?>
                          <div class="display-inline-mid">
                            <input type="radio" name="default" id="yes" class="display-inline-mid width-20px" checked="checked">
                            <label for="yes" class="display-inline-mid margin-top-10 default-cursor"><?php echo $text_yes; ?></label>
                          </div>
                          <div class="display-inline-mid">
                            <input type="radio" name="default" id="no" class="display-inline-mid width-20px" checked="checked">
                            <label for="no" class="display-inline-mid margin-top-10 default-cursor"><?php echo $text_no; ?></label>
                          </div>
                        <?php } ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
            </div>
            <div class="f-left margin-top-10 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>             
        </form>
          <?php echo $column_right; ?>
          <div class="clear"></div>   
        </div>    
      </section>
  <?php echo $content_bottom; ?>
  <script src="catalog/view/theme/ag-global-pharma/js/customdropdown.js"></script>
  <!--<script type="text/javascript">$("select").transformDD();</script>-->
<script type="text/javascript"><!--
// Sort the custom fields
$('.form-group[data-sort]').detach().each(function() {
  if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('.form-group').length) {
    $('.form-group').eq($(this).attr('data-sort')).before(this);
  }

  if ($(this).attr('data-sort') > $('.form-group').length) {
    $('.form-group:last').after(this);
  }

  if ($(this).attr('data-sort') < -$('.form-group').length) {
    $('.form-group:first').before(this);
  }
});
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
  var node = this;

  $('#form-upload').remove();

  $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

  $('#form-upload input[name=\'file\']').trigger('click');

  if (typeof timer != 'undefined') {
      clearInterval(timer);
  }

  timer = setInterval(function() {
    if ($('#form-upload input[name=\'file\']').val() != '') {
      clearInterval(timer);

      $.ajax({
        url: 'index.php?route=tool/upload',
        type: 'post',
        dataType: 'json',
        data: new FormData($('#form-upload')[0]),
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {
          $(node).button('loading');
        },
        complete: function() {
          $(node).button('reset');
        },
        success: function(json) {
          $(node).parent().find('.text-danger').remove();

          if (json['error']) {
            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
          }

          if (json['success']) {
            alert(json['success']);

            $(node).parent().find('input').attr('value', json['code']);
          }
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
      });
    }
  }, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});

$('.datetime').datetimepicker({
  pickDate: true,
  pickTime: true
});

$('.time').datetimepicker({
  pickDate: false
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
  $.ajax({
    url: 'index.php?route=account/account/country&country_id=' + this.value,
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value=""><?php echo $text_select; ?></option>';
      html2 = '<div data-value="" class="option"><?php echo $text_select; ?></div>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';
          html2 += '<div data-value="' + json['zone'][i]['zone_id'] + '" class="option">' + json['zone'][i]['name'] + '</div>';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
            }

            html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
        html2 += '<div data-value="0" class="option"><?php echo $text_none; ?></div>';
      }

      $('select[name=\'zone_id\']').html(html);
      $('.frm-custom-dropdown-option:last').html(html2);
      $('input.dd-txt:last').val('');
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
<?php echo $footer; ?>