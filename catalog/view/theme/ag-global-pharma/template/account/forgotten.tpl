<?php echo $header; ?>
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/inner-product.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/unique-widgets.css">
<!-- <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/media.css"> -->
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/forgot.css">
<?php echo $content_top; ?>
<?php echo $column_left; ?>
  <section class="reglar-panel" id="first-panel">
    <div class="heading-panel">
      <?php $bcount = count($breadcrumbs); ?>
      <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
      <?php if($bkey > 0): ?>
      <h2><?php echo $breadcrumb['text']; ?></h2>
      <?php if($bcount != ($bkey+1)): ?>
        <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
      <?php endif; ?>
    <?php else: ?>
      <?php echo $breadcrumb['text']; ?>
    <?php endif; ?>
      <?php } ?>
    </div>



    <!-- white panel -->
    <div class="full-panel big-forgot-panel">

      <div class="col-sm-8 forgot-panel">
        <p class="font-30"><?php echo $heading_title; ?></p>
        <p class="font-14"><?php echo $text_email; ?></p>

        <?php if ($error_warning) { ?>
        <br /><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
        <?php } ?>
        <!-- Forgot Password  -->
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="box-panel margin-top-20">
          <p class="title-content"><?php echo $text_your_email; ?></p>
          <div class="para-content ">
            <table class="width-100percent">
              <tbody>
                <tr>
                  <td class="width-200px">
                    <p class="font-15 font-lato f-right margin-right-20 margin-top-10"><?php echo $entry_email; ?></p>
                  </td>
                  <td>
                    <input type="text" name="email" value="" placeholder="<?php echo $entry_email; ?>" id="input-email" />
                  </td>
                </tr>                       
              </tbody>
            </table>
            <div class="margin-top-30">
              <button class="ag-button f-left" onclick="window.location.href='<?php echo $back; ?>';return false;"><?php echo $button_back; ?></button>
              <button class="ag-button f-right"><?php echo $button_continue; ?></button>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </form>
      </div>
      <?php echo $column_right; ?>
      <div class="clear"></div>
    </div>    
  </section>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>