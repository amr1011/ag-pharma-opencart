<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/order-history.css">
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <section class="bg-white" id="first-panel">
        <!-- breadcrumbs  -->
        <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
      <?php else: ?>
        <?php echo $breadcrumb['text']; ?>
      <?php endif; ?>
        <?php } ?>
        </div>


        <!-- white panel -->
        <div class="full-panel fixed-page history-panel">

          <!-- table panel  -->
          <div class="order-history">     
            <div class="box-panel">

              <p class="title-content"><?php echo $heading_title; ?></p>
              <div class="para-content padding-0px">
                <table class="tbl-wish tbl-order">
                  <thead>
                    <tr>
                      <th><?php echo $column_order_id; ?></th>
                      <th><?php echo $column_status; ?></th>
                      <th><?php echo $column_date_added; ?></th>
                      <th><?php echo $column_product; ?></th>
                      <th><?php echo $column_customer; ?></th>
                      <th><?php echo $column_total; ?></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($orders as $order) { ?>
                    <tr>
                      <td>#<?php echo $order['order_id']; ?></td>
                      <td><?php echo $order['status']; ?></td>
                      <td><?php echo $order['date_added']; ?></td>
                      <td><?php echo $order['products']; ?></td>
                      <td><?php echo $order['name']; ?></td>
                      <td><?php echo $order['total']; ?></td>                 
                      <td><a href="<?php echo $order['href']; ?>"><button class="ag-button"><i class="fa fa-eye font-20"></i></button></a></td>
                    </tr>
                    <?php } ?>              
                  </tbody>
                </table>
                <div class="text-right"><?php echo $pagination; ?></div>
              </div>
              
            </div>
            <div class="f-right margin-top-10 margin-bottom-30">
              <button class="ag-button" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_continue; ?></button>
            </div>
            <div class="clear"></div>
          </div>              
          <?php echo $column_right; ?>
          <div class="clear"></div>
        </div>    
      </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>