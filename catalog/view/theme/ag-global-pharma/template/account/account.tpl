<?php echo $header; ?>
  <?php echo $content_top; ?>
    <?php echo $column_left; ?>
      <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/account.css">
      <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/wish.css">
      <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/account-user.css">
      <section class="reglar-panel" id="first-panel">
      <div class="heading-panel">
        <?php $bcount = count($breadcrumbs); ?>
        <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
        <?php if($bkey > 0): ?>
        <h2><?php echo $breadcrumb['text']; ?></h2>
        <?php if($bcount != ($bkey+1)): ?>
          <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
        <?php endif; ?>
        <?php else: ?>
          <?php echo $breadcrumb['text']; ?>
        <?php endif; ?>
        <?php } ?>
      </div>
      
    <!-- white panel -->
    <div class="full-panel full-height account-user-full-panel">

      <div class="col-sm-8 wishlist">       
        <!-- wishlist -->
      <?php if ($success) { ?>
      <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
      <?php } ?>

        <div class="box-panel ">
          <p class="title-content"><?php echo $text_my_account; ?></p>
          <div class="para-content padding-0px">
            <table class="account-full-tbl">
              <tbody>
                <tr>
                  <td>
                    <a href="<?php echo $edit; ?>" class="f-left width-90percent"><?php echo $text_edit; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <a href="<?php echo $password; ?>" class="f-left width-90percent"><?php echo $text_password; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <a href="<?php echo $address; ?>" class="f-left width-90percent"><?php echo $text_address; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td><a href="<?php echo $wishlist; ?>" class="f-left width-90percent"><?php echo $text_wishlist; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>               
              </tbody>
            </table>
          </div>
          
        </div>

        <div class="box-panel margin-top-50">
          <p class="title-content"><?php echo $text_my_orders; ?></p>
          <div class="para-content padding-0px">
            <table class="account-full-tbl">
              <tbody>
                <tr>
                  <td><a href="<?php echo $order; ?>" class="f-left width-90percent"><?php echo $text_order; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td><a href="<?php echo $return; ?>" class="f-left width-90percent"><?php echo $text_return; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>
                <tr>
                  <td><a href="<?php echo $transaction; ?>" class="f-left width-90percent"><?php echo $text_transaction; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>               
              </tbody>
            </table>
          </div>        
        </div>
      
        <div class="box-panel margin-top-50 ">
          <p class="title-content"><?php echo $text_my_newsletter; ?></p>
          <div class="para-content padding-0px">
            <table class="account-full-tbl">
              <tbody>
                <tr>
                  <td><a href="<?php echo $newsletter; ?>" class="f-left width-90percent"><?php echo $text_newsletter; ?></a>
                    <i class="fa fa-arrow-right f-right"></i>
                    <div class="clear"></div>
                  </td>
                </tr>           
              </tbody>
            </table>
          </div>        
        </div>

      </div>              

      <!-- menu panel -->
      <?php echo $column_right; ?>    
      <div class="clear"></div>   
    </div>    
  </section>
  <?php echo $content_bottom; ?>
<?php echo $footer; ?>