<?php if (!isset($redirect)) { ?>
<table class="pricing">
  <thead>
    <tr>
      <th><?php echo $column_name; ?></th>
      <th><?php echo $column_model; ?></th>
      <th><?php echo $column_quantity; ?></th>
      <th><?php echo $column_price; ?></th>
      <th><?php echo $column_total; ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($products as $product) { ?>
    <tr>
      <td>
        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
        <?php foreach ($product['option'] as $option) { ?>
        <br />
        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
        <?php } ?>
        <?php if($product['recurring']) { ?>
          <br />
          <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
          <?php } ?>
      </td>
      <td><?php echo $product['model']; ?></td>
      <td><?php echo $product['quantity']; ?></td>
      <td><?php echo $product['price']; ?></td>
      <td><?php echo $product['total']; ?></td>
    </tr>
    <?php } ?>
    <?php foreach ($totals as $total) { ?>
    <tr>
      <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
      <td class="text-right"><?php echo $total['text']; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php echo $payment; ?>
<?php } else { ?>
<script type="text/javascript"><!--
location = '<?php echo $redirect; ?>';
//--></script>
<?php } ?>