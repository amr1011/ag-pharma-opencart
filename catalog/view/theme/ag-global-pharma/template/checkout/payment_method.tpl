<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<p class="title-body"><?php echo $text_payment_method; ?></p>
<div class="margin-left-25">
<?php foreach ($payment_methods as $payment_method) { ?>
<?php if ($payment_method['code'] == $code || !$code) { ?>
<?php $code = $payment_method['code']; ?>
<input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" class="margin-left-25 margin-top-10 width-20px" checked="checked" />
<?php } else { ?>
<input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>"  class="margin-left-25 margin-top-10 width-20px" />
<?php } ?>
<label class="margin-left-10" for="pay-type1"><?php echo $payment_method['title']; ?>
<?php if ($payment_method['terms']) { ?>
(<?php echo $payment_method['terms']; ?>)
<?php } ?></label><br />
<?php } ?>
<?php } ?>
  <p class="margin-left-25 margin-top-10"><strong><?php echo $text_comments; ?></strong></p><br/>
  <textarea row="5" class="width-80per" name="comment"><?php echo $comment; ?></textarea>
</div>
<div class="f-right">
<?php if ($text_agree) { ?>
<div class="f-left" style="margin-top:24px;">
<?php echo $text_agree; ?>
<?php if ($agree) { ?>
<input type="checkbox" name="agree" value="1" checked="checked" class="display-inline-mid width-20px margin-bottom-5"  />
<?php } else { ?>
<input type="checkbox" name="agree" value="1" class="display-inline-mid width-20px margin-bottom-5" />
<?php } ?>
</div>
&nbsp;&nbsp;
<?php } ?>
<button style="margin:10px 20px 0px 0px;" class="margin-top-10 f-left ag-button" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>"><strong><?php echo $button_continue; ?></strong></button>
</div>