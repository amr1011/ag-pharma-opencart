<?php echo $header; ?>
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/inner-product.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/unique-widgets.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/media.css">
<link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/accordion.css">
<?php echo $content_top; ?>
<?php echo $column_left; ?>
<section class="reglar-panel  padding-bottom-50" id="first-panel">
  <div class="heading-panel">
    <?php $bcount = count($breadcrumbs); ?>
    <?php foreach ($breadcrumbs as $bkey => $breadcrumb) { ?>
    <?php if($bkey > 0): ?>
    <h2><?php echo $breadcrumb['text']; ?></h2>
    <?php if($bcount != ($bkey+1)): ?>
      <i class="fa fa-angle-right font-20 margin-left-10 margin-right-10 white-color"></i>
    <?php endif; ?>
  <?php else: ?>
    <?php echo $breadcrumb['text']; ?>
  <?php endif; ?>
    <?php } ?>
  </div>

  <!-- white panel -->
  <div class="panel-group margin-top-50">
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <h3 class="font-30"><?php echo $heading_title; ?>
        <?php if ($weight) { ?>
        &nbsp;(<?php echo $weight; ?>)
        <?php } ?></h3>
  </div>

  <div class="panel-group">
    <div class="panel-body">
      <table class="pricing response-full">
        <thead>
          <tr>
            <th><?php echo $column_image; ?></th>
            <th><?php echo $column_name; ?></th>
            <th><?php echo $column_model; ?></th>
            <th><?php echo $column_quantity; ?></th>
            <th><?php echo $column_price; ?></th>
            <th><?php echo $column_total; ?></th>
          </tr>
        </thead>
        <tbody id="data">
          <?php foreach ($products as $product) { ?>
          <tr>
            <td><?php if ($product['thumb']) { ?>
              <div>
                <img src="<?php echo $product['thumb']; ?>">
              </div><?php } ?>
            </td>
            <td><?php echo $product['name']; ?>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?>
            </td>
            <td class="text-left"><?php echo $product['model']; ?></td>
            <td class="text-left data get-value-input">
              <input type="text"  id="quantity" id-qty="<?php echo $product['product_id']; ?>" name="quantity" value="<?php echo $product['quantity']; ?>" size="1" class="this-is-value width-145px " />
                
              <button href="#0"  class="add-button ag-button width-30px add_q" data-id="<?php echo $product['product_id']; ?>" data-quantity="<?php echo $product['quantity']; ?>" data-key="<?php echo $product['cart_id']; ?>" >+</button>
              <button href="#0" class="minus-button ag-button width-30px sub_q" data-id="<?php echo $product['cart_id']; ?>"  data-quantity="<?php echo $product['quantity']; ?>">-</button>
              <button href="#0" class="minus-button ag-button width-30px remove-to-cart" alt="Remove" title="Remove" cid="<?php echo $product['cart_id']; ?>">x</button>
            </td>
            <td id="price"><?php echo $product['price']; ?></td>
            <td id="total"><?php echo $product['total']; ?></td>
          </tr>
          <?php } ?>
          <?php foreach ($vouchers as $vouchers) { ?>
          <tr>
            <td></td>
            <td><?php echo $vouchers['description']; ?></td>
            <td></td>
            <td><div class="input-group btn-block" style="max-width: 200px;">
                    <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />
                    <span class="input-group-btn"><button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger" onclick="voucher.remove('<?php echo $vouchers['key']; ?>');"><i class="fa fa-times-circle"></i></button></span></div></td>
            <td><?php echo $vouchers['amount']; ?></td>
            <td><?php echo $vouchers['amount']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>

      <?php if (($coupon || $voucher || $reward || $shipping) && 1==2) { ?>
      <h2><?php echo $text_next; ?></h2>
      <p><?php echo $text_next_choice; ?></p>
      <div class="panel-group" id="accordion"><?php echo $coupon; ?><?php echo $voucher; ?><?php echo $reward; ?><?php echo $shipping; ?></div>
      <?php } ?>

      <div class="pricing1-div">
        <table class="pricing-ver1 response-320">         
          <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
              <td colspan="2">
                  <?php echo $product['name']; ?>
                  <?php if (!$product['stock']) { ?>
                  <span class="text-danger">***</span>
                  <?php } ?>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['reward']) { ?>
                  <br />
                  <small><?php echo $product['reward']; ?></small>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                  <?php } ?>
              </td>
            </tr>
            <tr>
              <td rowspan="4"><img src="<?php echo $product['thumb']; ?>"></td>
              <td><p><?php echo $column_model; ?></p>
                <p><?php echo $product['model']; ?></p>
                <div class="clear"></div>
              </td>
            </tr>
            <tr>
              <td><input type="text" />
                <button class="ag-button">+</button>
                <button class="ag-button">-</button>
              </td>
            </tr>
            <tr>
              <td><p><?php echo $column_price; ?></p>
                <p><?php echo $product['price']; ?></p>
                <div class="clear"></div>
              </td>
            </tr>
            <tr>
              <td><p><?php echo $column_total; ?></p>
                <p><?php echo $product['total']; ?></p>
                <div class="clear"></div>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="panel-group padding-20px">
    <div class="panel-body">
      <table class="pricing width-50per f-right">
              <tbody>
                <!-- Price Total -->
                <?php foreach ($totals as $total) { ?>
                <tr>
                    <td colspan="4" class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
                    <td class="text-right"><?php echo $total['text']; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <div class="clear"></div>
    </div>

    <div class="margin-top-20">

      <button class="ag-button f-left" onclick="window.location.href='<?php echo $continue; ?>';"><?php echo $button_shopping; ?></button>
    
      <button class="ag-button f-right" onclick="window.location.href='<?php echo $checkout; ?>';"><?php echo $button_checkout; ?></button>
      

     
      <div class="clear"></div>
    </div>

  </div>    
</section>
<?php echo $column_right; ?>
<?php echo $content_bottom; ?>
  <script>
    // accordion
    $(".panel-group .panel-heading").each(function(){
      var ps = $(this).next(".panel-collapse");
      var ph = ps.find(".panel-body").outerHeight();

      if(ps.hasClass("in")){
        $(this).find("h4").addClass("active")
      }

      $(this).find("a").off("click").on("click",function(e){
        e.preventDefault();
        ps.css({height:ph});

        if(ps.hasClass("in")){
          $(this).find("h4").removeClass("active")
          ps.removeClass("in");
        }else{
          $(this).find("h4").addClass("active")
          ps.addClass("in");
        }

        setTimeout(function(){
          ps.removeAttr("style")
        },500);
      });
    });
    $("select").transformDD();
  </script>
   <script src="catalog/view/theme/ag-global-pharma/js/inner-product-script.js"></script>
<?php echo $footer; ?>
