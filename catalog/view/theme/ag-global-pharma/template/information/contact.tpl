<?php echo $header; ?>
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/contact.css">
  <section class="contact-us-panel" id="first-panel">
    <div class="contact-us-panel-center">
      <div class="contact-us-panel-content">
        <h2 class="heading-text dark-green margin-bottom-30">Contact Us</h2>
        <p class="margin-bottom-30 font-15">For questions, comments and suggestions, get in touch with us today. Fill out the form below and one of our representatives will get back to you.</p>
        
        <div class="form-container">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <input type="text" placeholder="<?php echo $entry_name; ?>" id ="name" required class="contact-name" name="name" value="<?php echo $name; ?>" />
            <input type="email" placeholder="<?php echo $entry_email; ?>" id="email" required class="contact-email" name="email" value="<?php echo $email; ?>" />
            <input type="text" placeholder="Subject" required class="contact-subject" id="subject" name="subject" value="" />
            <textarea name="enquiry" id="enquiry"><?php echo $enquiry; ?></textarea>

            <button class="ag-button" id="send">Send</button>
          </form>
        </div>

        <div class="clear"></div>

        <div class="contact-text">
          <p class="address">2nd Floor, NVM Mall, Sayre Highway, Valencia City, Bukidnon, Philippines</p>
          <p class="telephone">Telefax: (088) 828.5513</p>
          <p class="mobile">Mobile: 0917.309.0915</p>
        </div>
      </div> <!-- first-panel-content -->
    </div> <!-- first-panel-center -->
  </section> <!-- first-panel -->
  <div id="map" class="contact-us-map"></div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="catalog/view/theme/ag-global-pharma/js/maps.js"></script>
<script type="text/javascript">
   
     $("#send").click(function(e){
      var name=$("#name").val(),
          email=$("#email").val(),
          subject=$("#subject").val(),
          enquiry=$("#enquiry").val();
   
          if (name != "" && email != "" && enquiry != "" && subject != "")
          {

             $.ajax({
            type:"post",
            url:"index.php?route=information/contact/addCustomerContacts",
            data:{ "name": name, "email": email ,"subject": subject, "enquiry": enquiry},
            success:function(data){
           
              }
            });

          }
     
       
     
  
     
});
  

 
  
            

</script>
<?php echo $footer; ?>