  <footer>    
    <div class="foot1">
      <ul class="footer-nav">       
        <li><a href="<?php echo $home; ?>">Home</a></li>
        <li><a href="<?php echo $about_us; ?>">About</a></li>
        <li><a href="<?php echo $products; ?>">Product</a></li>
        <li><a href="<?php echo $faq; ?>">Faq</a></li>
        <li><a href="<?php echo $contact_us; ?>">Contact Us</a></li>
      </ul>

      <div class="copyright-text"><p>2015 Copyright. All Rights Reserved</p></div>
    </div>

    <div class="foot2">
      <div class="back-to-top padding-right-10"><a href="#first-panel" class="back-to-top-button"><i class="fa fa-arrow-up font-15"></i> Back to Top</a></div>
      <div class="back-to-top "><a href="#first-panel" class="back-to-top-button-res">&uarr;</a></div>
      <div class="fas-logo">
        <p >Site By:</p>
        <a href="http://www.fascin8digital.com"><img src="catalog/view/theme/ag-global-pharma/images/fascin8-updated-white.png" alt="fascin8 logo" class="width-100px" /></a>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
    
  </footer>

  <!-- Scripts -->
  <script src="catalog/view/theme/ag-global-pharma/js/bootstrap.min.js"></script>
  <script src="catalog/view/theme/ag-global-pharma/js/responsiveslides.min.js"></script>
  <!-- // <script src='https://www.google.com/recaptcha/api.js'></script> -->
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
  <script src="catalog/view/theme/ag-global-pharma/js/maps.js"></script>-->
  <script src="catalog/view/theme/ag-global-pharma/js/script.js"></script>
  <script src="catalog/view/theme/ag-global-pharma/js/inner-product-script.js"></script>
  <script>
    //$("select").transformDD()
  </script>
</body>
</html>