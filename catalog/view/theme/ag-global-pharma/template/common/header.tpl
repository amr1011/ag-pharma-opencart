<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
  <!-- Meta Tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>
  <meta name="author" content="">
  
  <!-- Favicon -->
  <!-- <link rel="shortcut icon" href="../assets/ico/favicon.ico"> -->

  <!-- Site Title -->
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />

  <!-- Stylesheets -->
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/font-awesome.min.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/bootstrap.min.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/bootstrap-theme.min.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/commons.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/rslides.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/style.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/inner-product.css">
  <link rel="stylesheet" href="catalog/view/theme/ag-global-pharma/stylesheet/unique-widgets.css">
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <script src="catalog/view/theme/ag-global-pharma/js/jquery-2.1.4.min.js"></script>
  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>
</head>
<body>
  <header>
    <div class="logo">
      <?php if ($logo) { ?>
      <h1><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" alt=""></a></h1>
      <?php } else { ?>
      <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
      <?php } ?>
    </div> <!-- logo -->

    <nav class="main-nav">
      <ul>
        <li>
          <a href="#0" class="nav-icon">
            <!-- <i class="fa fa-bars"></i> -->
            <div class="bar top-bar"></div>
            <div class="bar mid-bar"></div>
            <div class="bar bot-bar"></div>
          </a>

          <ul class="child-nav">
            <li>
              <ul class="res-login">
                <?php if ($logged) { ?>
                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                <?php } else { ?>
                <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                <?php } ?>
              </ul>
            </li>
            <li><a href="<?php echo $home; ?>"<?php echo ($uri == 'home' || $uri == '') ? ' class="active"' : ''; ?>>Home</a></li>
            <li><a href="<?php echo $about_us; ?>"<?php echo ($uri == 'about_us') ? ' class="active"' : ''; ?>>About</a></li>
            <li><a href="<?php echo $products; ?>"<?php echo ($uri == 'list') ? ' class="active"' : ''; ?>>Product</a></li>
            <li><a href="<?php echo $faq; ?>"<?php echo ($uri == 'faq') ? ' class="active"' : ''; ?>>Faq</a></li>
            <li><a href="<?php echo $contact_us; ?>"<?php echo ($uri == 'contact') ? ' class="active"' : ''; ?>>Contact Us</a></li>
            <div class="clear"></div>
          </ul>

        </li>
      </ul>
    </nav> <!-- main-nav -->

    <div class="header-crumbs">
      <ul>
        <?php if ($logged) { ?>
          <li><a href="<?php echo $account; ?>" class="register"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
        <?php } else { ?>
          <li><a href="<?php echo $register; ?>" class="register"><?php echo $text_register; ?></a></li>
          <li><a href="<?php echo $login; ?>" class="login"><?php echo $text_login; ?></a></li>
        <?php } ?>
      </ul>

      <div class="checkout-container">
          <span class="cart-icon"><i class="fa fa-shopping-cart"></i><div class="notif"><?php echo $cart_products_count; ?></div></span>
          <?php
          
          ?>
          <label for="">You have <span class="blue-color"><?php echo $cart_products_count; ?> items</span></label>
          <button onclick="window.location.href='<?php echo $shopping_cart; ?>';">Check Out</button>
      </div>
    </div> <!-- header-crumbs -->

    <div class="clear"></div>
  </header> <!-- header -->