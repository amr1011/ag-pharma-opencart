
$(document).ready(function(){
   // google maps
    var map;
    var marker;
    var image = "catalog/view/theme/ag-global-pharma/images/index/map-marker.png";
    var latlng =  new google.maps.LatLng(7.9075279,125.0900369);
    var pinlatlng  = new google.maps.LatLng(7.9045843,125.0900262);
    var maptype_id = 'PHARMA_MAP';

    function initialize() {
      //set map grayscale
      var featureOpts = [{featureType: "all", elementType: "all", stylers:[{ saturation: -100 }] }];

      var mapOptions = {zoom: 17, center: latlng, disableDefaultUI: true, mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, maptype_id]
                },mapTypeId: maptype_id};

      map = new google.maps.Map(document.getElementById('map'),mapOptions);

      var styledMapOptions = {name: 'PHARMA MAP'};
      var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
      map.mapTypes.set(maptype_id, customMapType);
      addMarker();
    }

    function addMarker(){
      marker = new google.maps.Marker({
        icon: image,
        position: pinlatlng,
        map: map,
        draggable: false
      });
    }

    initialize();
});