$(document).ready(function(){
	$('.nav-show-button .ag-button').click(function(){
		$('.right-panel').toggleClass('addcart-show');
		$('.nav-show-button .fa').toggleClass('show-button-rotate');
	});

	

		var uiRemove = $('.remove-to-cart');
		uiRemove.off("click.Remove").on("click.Remove", function(e){
		var key = $(this).attr('cid');

		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',
			beforeSend: function() {
				$('#cart > button').button('loading');
			},
			complete: function() {
				$('#cart > button').button('reset');
			},
			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				setTimeout(function () {
					$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
					window.location.href = 'index.php?route=checkout/cart';
				}, 100);

/*				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > ul').load('index.php?route=common/cart/info ul li');
				}*/
			},
	        error: function(xhr, ajaxOptions, thrownError) {
	            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	        }
		});

		e.preventDefault();
	});
});