<?php
class ControllerProductList extends Controller {
	public function index() {
		$this->load->model('catalog/product');
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));
		$data = array();
		$products = $this->model_catalog_product->getProducts();

		if(is_array($products) && count($products) > 0) {
			foreach($products as $key => $product) {
				$products[$key]['image'] = HTTPS_SERVER.'image/'.$product['image'];
				$products[$key]['link'] = $this->url->link('product/product', 'product_id='.$product['product_id'], 'SSL');
				$attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);

				if(is_array($attributes) && count($attributes) > 0) {
					foreach($attributes as $key2 => $attribute) {
						if(isset($attribute['attribute']) && count($attribute['attribute']) > 0) {
							foreach($attribute['attribute'] as $key3 => $t) {
								$products[$key]['attributes'][strtolower($t['name'])] = $t;
							}
						}
					}
				}
			}
		}

		$data['products'] = $products;
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/list.tpl', $data));
	}
}