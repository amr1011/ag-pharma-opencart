<?php
class ControllerModuleTestimonials extends Controller {
	public function index() {
		$this->load->language('module/testimonials');
		$data['heading_title'] = $this->language->get('heading_title');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/testimonials.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/testimonials.tpl', $data);
		}
	}
}