<?php
class ControllerModuleContactus extends Controller {
	public function index() {
		$this->load->language('module/contact_us');
		$data['heading_title'] = $this->language->get('heading_title');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/contact_us.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/contact_us.tpl', $data);
		}
	}
}