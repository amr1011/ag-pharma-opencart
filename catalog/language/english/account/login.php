<?php
// Heading
$_['heading_title']                = 'Account Login';

// Text
$_['text_account']                 = 'ACCOUNT';
$_['text_login']                   = 'LOGIN';
$_['text_new_customer']            = 'New Customer';
$_['text_register']                = 'Register Account';
$_['text_register_account']        = 'By creating an account, you’ll be able to shop faster and more conveniently. You can also be up-to-date on the status of your orders and keep track of all the orders you have previously made.';
$_['text_returning_customer']      = 'Returning Customer';
$_['text_i_am_returning_customer'] = 'Login';
$_['text_forgotten']               = 'Forgotten Password';

// Entry
$_['entry_email']                  = 'E-mail Address';
$_['entry_password']               = 'Password';

// Error
$_['error_login']                  = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_attempts']               = 'Warning: Your account has exceeded allowed number of login attempts. Please try again in 1 hour.';
$_['error_approved']               = 'Warning: Your account requires approval before you can login.';