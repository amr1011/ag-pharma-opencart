<?php
// Heading
$_['heading_title']        = 'Register Account';

// Text
$_['text_account']         = 'ACCOUNT';
$_['text_register']        = 'REGISTRATION';
$_['text_account_already'] = '<p class="margin-top-20 margin-bottom-10 font-15">For us to easily accommodate your orders, we’ll need you to sign up. It’s free! Just fill out the registration form
below and verify your account. Happy ordering!
</p><p class="f-left margin-top-10">Already have an account? <a href="%s">Login Here</a>.</p>';
$_['text_your_details']    = 'Your Personal Details';
$_['text_your_address']    = 'Your Address';
$_['text_newsletter']      = 'Newsletter';
$_['text_your_password']   = 'Your Password';
$_['text_agree']           = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Customer Group';
$_['entry_firstname']      = 'First Name';
$_['entry_lastname']       = 'Last Name';
$_['entry_email']          = 'E-Mail Address';
$_['entry_telephone']      = 'Telephone Number';
$_['entry_fax']            = 'Fax Number';
$_['entry_company']        = 'Company';
$_['entry_address_1']      = 'Address 1';
$_['entry_address_2']      = 'Address 2';
$_['entry_postcode']       = 'Post Code';
$_['entry_city']           = 'City';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Region / State';
$_['entry_newsletter']     = 'Subscribe';
$_['entry_password']       = 'Password';
$_['entry_confirm']        = 'Password Confirm';

// Error
$_['error_exists']         = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']      = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']       = 'Last Name must be between 1 and 32 characters!';
$_['error_email']          = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']      = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']           = 'City must be between 2 and 128 characters!';
$_['error_postcode']       = 'Postcode must be between 2 and 10 characters!';
$_['error_country']        = 'Please select a country!';
$_['error_zone']           = 'Please select a region / state!';
$_['error_custom_field']   = '%s required!';
$_['error_password']       = 'Password must be between 4 and 20 characters!';
$_['error_confirm']        = 'Password confirmation does not match password!';
$_['error_agree']          = 'Warning: You must agree to the %s!';