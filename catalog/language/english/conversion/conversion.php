<?php
// Text
$_['conversion'] = $this->getCountryByIP()->geoplugin_currencyCode;



 function getCountryByIP() {
		$ip = $this->get_client_ip();
		$details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
		
		return $details;
	}

	 function get_client_ip(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		if($ip == '::1'){
			//set what ip you want
			$ip = '119.92.244.146';
		}

		return $ip;
	}