<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      
      <h1>Customer Contacts</h1>
      <ul class="breadcrumb">
        
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> </h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  
                  <td class="text-left">Name</td>
                  <td class="text-left">Subject</td>
                  <td class="text-left">Email</td>
                  <!--<td class="text-left">Action</td>-->
                </tr>
              </thead>
              <tbody>
                
                <?php foreach ($customers as $customer) { ?>
                <tr>
                  
                  <td class="text-left"><?php echo $customer['name']; ?></td>
                  <td class="text-right"><?php echo $customer['subject']; ?></td>
                  <td class="text-right"><?php echo $customer['email']; ?></td>
                 <!-- <td class="text-right"><a href="<?php echo $customer['id']; ?>" data-toggle="tooltip" title="view" class="btn btn-primary"><i class="fa fa-eye"></i></a></td> -->
                </tr>
                <?php } ?>
             
                <tr>
                  <td class="text-center" colspan="4"></td>
                </tr>
               
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"></div>
          <div class="col-sm-6 text-right"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>