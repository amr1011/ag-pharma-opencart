<?php
// HTTP
//Local
/*define('HTTP_SERVER', 'http://localhost/ag-pharma/admin/');
define('HTTP_CATALOG', 'http://localhost/ag-pharma/');*/

//Live
define('HTTP_SERVER', 'http://staging.cr8vsolutions.com/ag-pharma/admin/');
define('HTTP_CATALOG', 'http://staging.cr8vsolutions.com/ag-pharma/');

// HTTPS
/*define('HTTPS_SERVER', 'http://localhost/ag-pharma/admin/');
define('HTTPS_CATALOG', 'http://localhost/ag-pharma/');*/

//Live
define('HTTPS_SERVER', 'http://staging.cr8vsolutions.com/ag-pharma/admin/');
define('HTTPS_CATALOG', 'http://staging.cr8vsolutions.com/ag-pharma/');

// DIR
/*define('DIR_APPLICATION', 'C:/wamp64/www/ag-pharma/admin/');
define('DIR_SYSTEM', 'C:/wamp64/www/ag-pharma/system/');
define('DIR_LANGUAGE', 'C:/wamp64/www/ag-pharma/admin/language/');
define('DIR_TEMPLATE', 'C:/wamp64/www/ag-pharma/admin/view/template/');
define('DIR_CONFIG', 'C:/wamp64/www/ag-pharma/system/config/');
define('DIR_IMAGE', 'C:/wamp64/www/ag-pharma/image/');
define('DIR_CACHE', 'C:/wamp64/www/ag-pharma/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/wamp64/www/ag-pharma/system/storage/download/');
define('DIR_LOGS', 'C:/wamp64/www/ag-pharma/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/wamp64/www/ag-pharma/system/storage/modification/');
define('DIR_UPLOAD', 'C:/wamp64/www/ag-pharma/system/storage/upload/');
define('DIR_CATALOG', 'C:/wamp64/www/ag-pharma/catalog/');*/

//LIVE
define('DIR_APPLICATION', '/home2/creigvso/public_html/staging/ag-pharma/admin/');
define('DIR_SYSTEM', '/home2/creigvso/public_html/staging/ag-pharma/system/');
define('DIR_LANGUAGE', '/home2/creigvso/public_html/staging/ag-pharma/admin/language/');
define('DIR_TEMPLATE', '/home2/creigvso/public_html/staging/ag-pharma/admin/view/template/');
define('DIR_CONFIG', '/home2/creigvso/public_html/staging/ag-pharma/system/config/');
define('DIR_IMAGE', '/home2/creigvso/public_html/staging/ag-pharma/image/');
define('DIR_CACHE', '/home2/creigvso/public_html/staging/ag-pharma/system/storage/cache/');
define('DIR_DOWNLOAD', '/home2/creigvso/public_html/staging/ag-pharma/system/storage/download/');
define('DIR_LOGS', '/home2/creigvso/public_html/staging/ag-pharma/system/storage/logs/');
define('DIR_MODIFICATION', '/home2/creigvso/public_html/staging/ag-pharma/system/storage/modification/');
define('DIR_UPLOAD', '/home2/creigvso/public_html/staging/ag-pharma/system/storage/upload/');
define('DIR_CATALOG', '/home2/creigvso/public_html/staging/ag-pharma/catalog/');


// DB
//Local
/*define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'creigvso_sm_aura');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');*/


//Live
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'creigvso_smauser');
define('DB_PASSWORD', 'F![uZ4I@6C$P');
define('DB_DATABASE', 'creigvso_sm_aura');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');