<?php
// Heading
$_['heading_title']    = 'Testimonials';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified testimonials module!';
$_['text_edit']        = 'Edit Testimonials Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify testimonials module!';