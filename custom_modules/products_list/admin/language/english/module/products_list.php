<?php
// Heading
$_['heading_title']    = 'Products List';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified products list module!';
$_['text_edit']        = 'Edit Products List Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify products list module!';