<?php
class ControllerModuleProductslist extends Controller {
	public function index() {
		$this->load->language('module/products_list');
		$this->load->model('catalog/product');

		$data['heading_title'] = $this->language->get('heading_title');
		$products = $this->model_catalog_product->getProducts();

		if(is_array($products) && count($products) > 0) {
			foreach($products as $key => $product) {
				$products[$key]['image'] = HTTPS_SERVER.'/image/'.$product['image'];
				$products[$key]['link'] = $this->url->link('product/product', 'product_id='.$product['product_id'], 'SSL');
				$attributes = $this->model_catalog_product->getProductAttributes($product['product_id']);

				if(is_array($attributes) && count($attributes) > 0) {
					foreach($attributes as $attribute) {
						if(is_array($attribute['attribute']) && count($attribute['attribute']) > 0) {
							foreach($attribute['attribute'] as $attr) {
								$products[$key]['attributes'][strtolower($attr['name'])] = $attr['text'];
							}
						}
					}
				}
			}
		}

		$data['products'] = $products;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/products_list.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/products_list.tpl', $data);
		}
	}
}